package com.localramu.android.app.background;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.JobsDataSource;
import com.localramu.common.messages.enums.GetJobRequestType;
import com.localramu.common.messages.model.Job;
import com.localramu.common.messages.service.request.GetJobRequest;
import com.localramu.common.messages.service.response.GetJobResponse;
import com.localramu.common.url.UrlDefs;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by mohitgupta on 28/02/16.
 */
public class GetJobProcess {

    private String mJobId;
    private JobsDataSource mJobsDataSource;
    private Context mContext;

    public GetJobProcess(String jobId, Context context) {
        mJobId = jobId;
        mContext = context;
        mJobsDataSource = new JobsDataSource(context);
    }

    public Job getJob() {
        Job job = null;
        try{
            String deviceUserId = Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            RestTemplate restTemplate = new RestTemplate();
            GetJobRequest getJobRequest = new GetJobRequest();
            getJobRequest.setJobId(mJobId);
            getJobRequest.setGetJobRequestType(GetJobRequestType.BY_JOB_ID);


            restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
            JSONObject json = new JSONObject();
            Gson gson = new Gson();


            Log.i("Request", gson.toJson(getJobRequest));
            GetJobResponse response = restTemplate.postForObject(UrlDefs.GET_JOB_URL,
                    getJobRequest, GetJobResponse.class);
            if(response.isSuccess()) {
                List<Job> jobList = response.getJobList();
                if(jobList != null && jobList.size() > 0) {
                    job  = jobList.get(0);
                    mJobsDataSource.write();
                    mJobsDataSource.updateJob(job);
                    mJobsDataSource.close();
                }
            }
            Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return job;

    }
}
