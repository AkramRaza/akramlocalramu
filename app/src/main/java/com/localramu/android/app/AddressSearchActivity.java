package com.localramu.android.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.localramu.android.app.adapter.AddressArrayAdapter;
import com.localramu.android.app.background.GeoPointTask;
import com.localramu.android.app.db.AddressDataSource;
import com.localramu.android.app.defs.GeneralDefs;
import com.localramu.android.app.models.Address;
import com.localramu.android.app.utils.GPSService;

import java.util.ArrayList;
import java.util.List;

public class AddressSearchActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "AddressSearchActivity";
    private SearchView mSearchView;
    private ListView mListView ,mSavedAddressListView;
    //private RecyclerView mRecyclerView;
    //private RecyclerView.Adapter mAdapter;
    //private RecyclerView.LayoutManager mLayoutManager;
    private ProgressDialog progressDialog;
    private GoogleApiClient mGoogleApiClient;
    private LatLngBounds latLngBounds;
    private AutocompleteFilter autocompleteFilter;
    private ProgressBar mLocationProgress;
    private TextView mUseGPSLabel;
    private AddressDataSource addressDataSource ;
    Button saveAddress;
    TextView add1;
    TextView add2;
    TextView add3;
    TextView add4;
    String addrs1,addrs2,addrs3,addrs4;
    String fullUserAddress;
    String LOCALRAMUPREF = "LocalRamuPref" ;
    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedpreferences = getSharedPreferences(LOCALRAMUPREF, Context.MODE_PRIVATE);
        saveAddress=(Button)findViewById(R.id.saveAddress);
        add1=(TextView) findViewById(R.id.addr1);
        add2=(TextView) findViewById(R.id.addr2);
        add3=(TextView) findViewById(R.id.addr3);
        add4=(TextView) findViewById(R.id.addr4);
        if(sharedpreferences.getString("fullUserAddress","")!=null){
            add1.setText(sharedpreferences.getString("flatNo",""));
            add2.setText(sharedpreferences.getString("streetAddress",""));
            add3.setText(sharedpreferences.getString("locality",""));
            add4.setText(sharedpreferences.getString("city",""));

        }

        saveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addrs1=add1.getText().toString().trim();
                addrs2=add2.getText().toString().trim();
                addrs3=add3.getText().toString().trim();
                addrs4=add4.getText().toString().trim();
                if(addrs1.equalsIgnoreCase("")||addrs2.equalsIgnoreCase("")||addrs3.equalsIgnoreCase("")||addrs4.equalsIgnoreCase("")) {
                    Snackbar.make(v, "Please fill all feilds to save your address", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }else{
                    fullUserAddress = addrs1+"," + addrs2+"," + addrs3+"," + addrs4+".";
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("flatNo",addrs1 );
                    editor.putString("streetAddress",addrs2);
                    editor.putString("locality",addrs3);
                    editor.putString("city",addrs4 );
                    editor.putString("fullUserAddress",fullUserAddress );
                    editor.commit();
                    Snackbar.make(v, "Thanks for saving your address", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    Log.e("fullUserAddres*********",fullUserAddress);
                    Log.e("fullUserAddres**SP*****",sharedpreferences.getString("fullUserAddress",""));
                    add1.setText("");
                    add2.setText("");
                    add3.setText("");
                    add4.setText("");
                    Intent intent= new Intent(AddressSearchActivity.this,DashboardActivity.class);
                    startActivity(intent);
                }
                /*AddressDataSource addressDataSource = new AddressDataSource(getApplicationContext());
                addressDataSource.write();
                addressDataSource.createAddress();
                addressDataSource.close();*/
            }
        });
        mapResource();

    }


    private void mapResource() {
        addressDataSource = new AddressDataSource(getApplicationContext());
        mSearchView = (SearchView) findViewById(R.id.address_search_view);

        mListView = (ListView) findViewById(R.id.address_list);
        mSavedAddressListView = (ListView) findViewById(R.id.saved_address_list);

        mLocationProgress = (ProgressBar) findViewById(R.id.loc_fetching_progress);
        mUseGPSLabel = (TextView) findViewById(R.id.use_gps_label);
        setSavedAddressListAdapter();
        //mRecyclerView = (RecyclerView) findViewById(R.id.address_list);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        //mLayoutManager = new LinearLayoutManager(this);
        //mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));


        mGoogleApiClient = new GoogleApiClient
                .Builder( this )
                .enableAutoManage( this, 0, this )
                .addApi( Places.GEO_DATA_API )
                .addApi( Places.PLACE_DETECTION_API )
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        latLngBounds =  new LatLngBounds(
                new LatLng(8.0, 97.0),
                new LatLng(35.0, 68.0));
        autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)

                .build();

       // String[] placesList = {"Delhi", "Mumbai", "Chennai", ""};
       // mAdapter = new AddressListAdapter(Arrays.asList(placesList));
       // mRecyclerView.setAdapter(mAdapter);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                int length = newText.length();
                if (length % 3 == 0) {
                    findplaces(newText);
                }
                return false;
            }
        });

        mListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view,
                                            int position, long id) {
                        Log.i(TAG, "Hurray " + position);
                        // TODO Auto-generated method stub
                        GeneralDefs.currentSelectedAddress = (Address) mListView.getItemAtPosition(position);

                        new GeoPointTask(GeneralDefs.currentSelectedAddress, getApplicationContext()).execute();
                        setResult(Activity.RESULT_OK, getReturnIntent());
                        finish();
                    }
                }
        );

        mSavedAddressListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view,
                                            int position, long id) {
                        Log.i(TAG, "Hurray " + position);
                        // TODO Auto-generated method stub
                        GeneralDefs.currentSelectedAddress = (Address)mSavedAddressListView.getItemAtPosition(position);
                        addressDataSource.write();
                        addressDataSource.createAddress(GeneralDefs.currentSelectedAddress);
                        addressDataSource.close();
                        setResult(Activity.RESULT_OK, getReturnIntent());
                        finish();
                    }
                }
        );
    }

    public void useGPSLocation(View view) {
        view.setEnabled(false);
        mLocationProgress.setVisibility(View.VISIBLE);
        mUseGPSLabel.setVisibility(View.GONE);

        GPSService gpsService = new GPSService(AddressSearchActivity.this.getApplicationContext(),
                AddressSearchActivity.this);
        gpsService.getLocation();
        Address address =  gpsService.getLocationAddress();
        if(address != null) {
            GeneralDefs.currentSelectedAddress = address;
            AddressDataSource addressDataSource = new AddressDataSource(getApplicationContext());
            addressDataSource.write();
            addressDataSource.createAddress(address);
            addressDataSource.close();
            setResult(Activity.RESULT_OK, getReturnIntent());
            finish();
        } else {
            view.setEnabled(true);
            mLocationProgress.setVisibility(View.GONE);
            mUseGPSLabel.setVisibility(View.VISIBLE);
        }

     }

    private void setSavedAddressListAdapter() {
        AddressDataSource addressDataSource = new AddressDataSource(getApplicationContext());
        addressDataSource.read();
        List<Address> savedAddresses = addressDataSource.getAllAddress();
        addressDataSource.close();
        AddressArrayAdapter adapter = new AddressArrayAdapter(AddressSearchActivity.this, savedAddresses);

        mSavedAddressListView.setAdapter(adapter);

    }

    private void findplaces(String query) {

        PendingResult<AutocompletePredictionBuffer> result =
                Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, query,
                        latLngBounds, autocompleteFilter);

        result.setResultCallback( new ResultCallback<AutocompletePredictionBuffer>() {
            @Override
            public void onResult( AutocompletePredictionBuffer places ) {
                List<Address> placesList = new ArrayList<Address>();

                for(AutocompletePrediction place : places) {
                    Address address = new Address();
                    address.setAddressPrimary(place.getPrimaryText(new StyleSpan(Typeface.NORMAL)).toString());
                    address.setAddressSecondary(place.getSecondaryText(new StyleSpan(Typeface.NORMAL)).toString());
                    placesList.add(address);
                }
                places.release();
                AddressArrayAdapter adapter = new AddressArrayAdapter(AddressSearchActivity.this, placesList);

                // Assign adapter to ListView
                mListView.setAdapter(adapter);

                //mAdapter = new AddressListAdapter(placesList);
                //mRecyclerView.setAdapter(mAdapter);
                //mRecyclerView.invalidate();
               // mRecyclerView.setAdapter(placesList);
                Log.i(TAG, String.valueOf(placesList));
            }

        });


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if( mGoogleApiClient != null )
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }



    private Intent getReturnIntent() {
        String addressSecondary = GeneralDefs.currentSelectedAddress.getAddressSecondary();
        String[] addressSplit = addressSecondary.split(",");
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", addressSplit[0]);

        return returnIntent;
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class GetCurrentLocationTask extends AsyncTask<Void, Void, Address> {

        private View mView;
        GetCurrentLocationTask(View view) {
            this.mView = view;
        }

        @Override
        protected Address doInBackground(Void... params) {

            GPSService gpsService = new GPSService(AddressSearchActivity.this.getApplicationContext(),
                    AddressSearchActivity.this);
            gpsService.getLocation();
            return gpsService.getLocationAddress();
        }

        @Override
        protected void onPostExecute(final Address address) {
            if(address != null) {
                GeneralDefs.currentSelectedAddress = address;
                AddressDataSource addressDataSource = new AddressDataSource(getApplicationContext());
                addressDataSource.write();
                addressDataSource.createAddress(address);
                addressDataSource.close();
                setResult(Activity.RESULT_OK, getReturnIntent());
                finish();
            } else {
                mView.setEnabled(true);
                mLocationProgress.setVisibility(View.GONE);
                mUseGPSLabel.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            mView.setEnabled(true);
        }
    }

    private void saveAddress(Address address) {
        AddressDataSource addressDataSource = new AddressDataSource(getApplicationContext());
        addressDataSource.write();
        addressDataSource.createAddress(address);
        addressDataSource.close();
    }
}
