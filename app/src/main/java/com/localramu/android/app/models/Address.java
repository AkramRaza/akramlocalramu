package com.localramu.android.app.models;

/**
 * Created by mohitgupta on 16/02/16.
 */
public class Address {


    private String addressPrimary;
    private String addressSecondary;
    private String locality;
    private String pinCode;
    private String city;
    private double latitude;
    private double longitude;
    private String landmark;
    private String mobileNumber;
    private String id;

    public Address() {

    }

    public Address(String addressPrimary , String addressSecondary,
                   String locality, String pinCode, String city,
                   double latitude, double longitude) {
        this.addressPrimary = addressPrimary;
        this.addressSecondary = addressSecondary;
        this.locality = locality;
        this.pinCode = pinCode;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public String getAddressPrimary() {
        return addressPrimary;
    }

    public void setAddressPrimary(String addressPrimary) {
        this.addressPrimary = addressPrimary;
    }

    public String getAddressSecondary() {
        return addressSecondary;
    }

    public void setAddressSecondary(String addressSecondary) {
        this.addressSecondary = addressSecondary;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
