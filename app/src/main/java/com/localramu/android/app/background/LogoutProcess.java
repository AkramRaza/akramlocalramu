package com.localramu.android.app.background;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.UserLoginActivity;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.model.UserInfo;
import com.localramu.common.messages.service.request.LogoutRequest;
import com.localramu.common.messages.service.response.LogoutResponse;
import com.localramu.common.url.UrlDefs;


public class LogoutProcess  extends AsyncTask<Void, Void, StatusCode>{

	private Context context;
	private Activity activity;

	public LogoutProcess(Context context, Activity activity) {
		this.context = context;
		this.activity = activity;
	}

	@Override
	protected StatusCode doInBackground(Void... params) {
		StatusCode statusCode = StatusCode.UNKNOWN;;
		try{
			UserInfo userInfo = Utilities.getUserInfo(context);
			if(userInfo != null) {

				RestTemplate restTemplate = new RestTemplate();
				LogoutRequest logoutRequest = new LogoutRequest();
				logoutRequest.setUserInfo(userInfo);

				restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());

				Gson gson = new Gson();
				Log.i("Request",  gson.toJson(logoutRequest));

				LogoutResponse response = restTemplate.postForObject(UrlDefs.LOGOUT_URL, 
						logoutRequest, LogoutResponse.class);
				statusCode = response.getStatusCode();
				Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());

			}
		}
		catch(Exception e){
			e.printStackTrace();
			statusCode = StatusCode.ERROR_OCCURRED;
		}

		return statusCode;
	}

	@Override
	protected void onPostExecute(StatusCode statusCode) {

		if(statusCode == StatusCode.SUCCESS) {
			try {
				UserInfoDataSource userInfoDataSource = new UserInfoDataSource(context);
				userInfoDataSource.write();
				userInfoDataSource.logout();
				userInfoDataSource.close();
				SharedPreferences preferences = context.getSharedPreferences("LocalRamuPref", 0);
				SharedPreferences.Editor editor = preferences.edit();
				editor.clear();
				editor.commit();
				Intent intent = new Intent(context,UserLoginActivity.class);
				activity.startActivity(intent); 

				Intent broadcastIntent = new Intent();
				broadcastIntent.setAction("com.localramu.partner.app.ACTION_LOGOUT");
				context.sendBroadcast(broadcastIntent);
				activity.finish();
			} catch(Exception e) {

			}
		}
	}
}
