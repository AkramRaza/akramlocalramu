package com.localramu.android.app.defs;

import com.localramu.android.app.models.Address;
import com.localramu.android.app.models.LaundryPriceModel;
import com.localramu.android.app.utils.AppConfig;

public class GeneralDefs {
	
	public static boolean loggingEnabled = false;
	public static StringBuilder logMessage;
	public static boolean isQueueEmpty = false;
	public static Object internetLock = new Object();
	public static Object queueLock = new Object();
	public static boolean isInternetAvailable = true;
	public static boolean power_on = true;
	public static Boolean timeout = true;

	public static Address currentSelectedAddress = null;
	public static AppConfig appConfig = null;


	public static final String APP_CONFIG_FILE = "CustomerAppConfig.json";



}
