package com.localramu.android.app.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.defs.TableDefs;
import com.localramu.android.app.models.Address;
import com.localramu.android.app.utils.Logger;
import com.localramu.common.messages.model.UserInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mohitgupta on 16/02/16.
 */
public class AddressDataSource {

    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private final String ADDRESS_TYPE_CURRENT = "CurrentAddress";
    private final String ADDRESS_TYPE_SAVED = "SavedAddress";

    private String[] allCoulumns = {TableDefs.COLUMN_ID,TableDefs.COLUMN_ADDRESS_TYPE, TableDefs.COLUMN_ADDRESS_PRIMARY, TableDefs.COLUMN_ADDRESS_SECONDARY,
            TableDefs.COLUMN_LOCALITY, TableDefs.COLUMN_CITY, TableDefs.COLUMN_LATITUDE, TableDefs.COLUMN_LONGITUDE, TableDefs.COLUMN_LANDMARK, TableDefs.COLUMN_MOBILE_NUMBER};
    public AddressDataSource(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void write() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void read() throws SQLException {
        database = dbHelper.getReadableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public boolean createAddress(Address address) {

        updateAllAddressAsSaved();
        ContentValues values = new ContentValues();
        values.put(TableDefs.COLUMN_ADDRESS_TYPE, ADDRESS_TYPE_CURRENT);
        if (address.getAddressPrimary() != null) {
            values.put(TableDefs.COLUMN_ADDRESS_PRIMARY, address.getAddressPrimary());
        }
        String addressId = String.valueOf(System.currentTimeMillis());
        values.put(TableDefs.COLUMN_ID, addressId);
        values.put(TableDefs.COLUMN_ADDRESS_SECONDARY, address.getAddressSecondary());
        values.put(TableDefs.COLUMN_LOCALITY, address.getLocality());
        values.put(TableDefs.COLUMN_CITY, address.getCity());
        values.put(TableDefs.COLUMN_LATITUDE, address.getLatitude());
        values.put(TableDefs.COLUMN_LONGITUDE, address.getLongitude());
        long id;
        try {
            id = database.insert(TableDefs.TABLE_ADDRESS, null, values);
        } catch (Exception e) {
            Log.i("AddressDataSource", "Address already saved");
            id = -1;
        }

        if (id == 1) {
            Logger.z(4, "AddressDataSource", "Created a new address successfully  " + id);
            return true;
        } else {
            Logger.z(4, "AddressDataSource", "Address Creation Unsuccessfull " + id);
            updateAddress(address.getAddressPrimary(), ADDRESS_TYPE_CURRENT);
            return false;
        }
    }




    public void updateAddress(int id, String addressType) {
        ContentValues values = new ContentValues();
        values.put(TableDefs.COLUMN_ADDRESS_TYPE, addressType);
        database.update(TableDefs.TABLE_ADDRESS, values, TableDefs.COLUMN_ID + "=\"" + id + "\"", null);
    }

    public void updateAddress(String id, String primaryAddress, String landmark, String mobile) {
        ContentValues values = new ContentValues();
        values.put(TableDefs.COLUMN_ADDRESS_PRIMARY, primaryAddress);
        values.put(TableDefs.COLUMN_LANDMARK, landmark);
        values.put(TableDefs.COLUMN_MOBILE_NUMBER, mobile);
        database.update(TableDefs.TABLE_ADDRESS, values, TableDefs.COLUMN_ID + "=\"" + id + "\"", null);
        Log.i("Address Update", id + "," + primaryAddress);
    }

    public void updateAddress(String addressPrimary, String addressType) {
        ContentValues values = new ContentValues();
        values.put(TableDefs.COLUMN_ADDRESS_TYPE, addressType);
        database.update(TableDefs.TABLE_ADDRESS, values, TableDefs.COLUMN_ADDRESS_PRIMARY + "=\"" + addressPrimary + "\"", null);
    }


    public void updateAllAddressAsSaved(int id) {
        ContentValues values = new ContentValues();
        values.put(TableDefs.COLUMN_ADDRESS_TYPE, ADDRESS_TYPE_SAVED);
        database.update(TableDefs.TABLE_ADDRESS, values, TableDefs.COLUMN_ID + "!=\"" + id + "\"", null);
    }

    public void updateAllAddressAsSaved() {
        ContentValues values = new ContentValues();
        values.put(TableDefs.COLUMN_ADDRESS_TYPE, ADDRESS_TYPE_SAVED);
        database.update(TableDefs.TABLE_ADDRESS, values, null, null);
    }


    private Address getAddressBasedOnPrimartyAddress(String addressPrimary) {
        Cursor cursor = database.query(TableDefs.TABLE_ADDRESS, allCoulumns, TableDefs.COLUMN_ADDRESS_PRIMARY + "=\"" + addressPrimary + "\"",
                null, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        else
            return null;
        if (cursor.getCount() > 0){
            Address address = getAddress(cursor);
            return address;
        }
        else
            return null;
    }

    public Address getCurrentAddress() {

        Cursor cursor = database.query(TableDefs.TABLE_ADDRESS, allCoulumns, TableDefs.COLUMN_ADDRESS_TYPE +"=\""+ADDRESS_TYPE_CURRENT+"\"",
                null, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        else
            return null;
        if (cursor.getCount() > 0){
            Address address = getAddress(cursor);
            if(cursor.getCount() != 1) {
                updateAllAddressAsSaved(cursor.getInt(cursor.getColumnIndex(TableDefs.COLUMN_ID)));
                Log.i("AddressDataSource","More than one address is marked as current");
            }
            return address;
        }
        else
            return null;

    }

    public List<Address> getSavedAddress() {
        List<Address> addressList = new ArrayList<Address>();
        Cursor cursor = database.query(TableDefs.TABLE_ADDRESS, allCoulumns, TableDefs.COLUMN_ADDRESS_TYPE +"=\'"+ADDRESS_TYPE_SAVED+"\'",
                null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Address address = getAddress(cursor);
                Log.i("Saved Address", String.valueOf(address.getAddressPrimary()));
                addressList.add(address);
                cursor.moveToNext();
            }
        }
        return addressList;

    }

    public List<Address> getAllAddress() {
        List<Address> addressList = new ArrayList<Address>();
        Cursor cursor = database.query(TableDefs.TABLE_ADDRESS, allCoulumns, null,
                null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Address address = getAddress(cursor);
                Log.i("All Address", String.valueOf(address.getAddressPrimary()));
                addressList.add(address);
                cursor.moveToNext();
            }
        }
        return addressList;

    }

    private Address getAddress(Cursor cursor) {
        Address address = new Address();
        address.setId(cursor.getString (cursor.getColumnIndex(TableDefs.COLUMN_ID)));
        address.setAddressPrimary(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_ADDRESS_PRIMARY)));
        address.setAddressSecondary(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_ADDRESS_SECONDARY)));
        address.setLocality(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_LOCALITY)));
        address.setCity(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_CITY)));
        address.setLatitude(cursor.getDouble(cursor.getColumnIndex(TableDefs.COLUMN_LATITUDE)));
        address.setLongitude(cursor.getDouble(cursor.getColumnIndex(TableDefs.COLUMN_LONGITUDE)));
        address.setLandmark(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_LANDMARK)));
        address.setMobileNumber(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_MOBILE_NUMBER)));

        return address;
    }

}
