package com.localramu.android.app.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.localramu.android.app.background.GcmTokenUpdateProcess;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.model.UserInfo;


public class GcmRegistrationIntentService  extends IntentService {

	private static final String TAG = "GcmRegistrationIntentService";

	public GcmRegistrationIntentService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		try {
			String token = null;
			UserInfo userInfo = Utilities.getUserInfo(this);
			if(userInfo != null) {
				if(userInfo.getGcmId() == null || userInfo.getGcmId().equals("")) {
					InstanceID instanceID = InstanceID.getInstance(this);
					token = instanceID.getToken("714527511057",
							GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
					userInfo.setGcmId(token);
				}
				if(!userInfo.isGcmUpdatedOnServer()) {
					GcmTokenUpdateProcess gcmTokenUpdateProcess = new GcmTokenUpdateProcess(userInfo, this);
					gcmTokenUpdateProcess.updateToken();
				}

				Log.i(TAG, "GCM Registration Token: " + token);
			}
		} catch (Exception e) {
			Log.d(TAG, "Failed to complete token refresh", e);
		}
	}



}
