package com.localramu.android.app.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.localramu.android.app.R;
import com.localramu.android.app.models.LaundryPriceModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mohitgupta on 06/03/16.
 */
public class LaundaryPriceAdapter extends ArrayAdapter<LaundryPriceModel> {

    private int tabPosition;
    public LaundaryPriceAdapter(Context context, List<LaundryPriceModel> values, int tabPosition) {
        super(context, 0, values);
        this.tabPosition = tabPosition;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        LaundryPriceModel priceModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.component_laundary_price_item, parent, false);
        }
        // Lookup view for data population
        TextView item = (TextView) convertView.findViewById(R.id.item);
        TextView price = (TextView) convertView.findViewById(R.id.price);

        String priceValue = null;
        switch(tabPosition) {
            case 1:
                priceValue = priceModel.getWashAndIron();
                Log.e("====priceValue1===>",""+priceValue);
                break;
            case 2:
                priceValue = priceModel.getWashPrice();
                Log.e("====priceValue2===>",""+priceValue);
                break;
            case 3:
                priceValue = priceModel.getIronPrice();
                Log.e("====priceValue3===>",""+priceValue);
                break;
            case 4:
                priceValue = priceModel.getDryWashPrice();
                Log.e("====priceValue4===>",""+priceValue);
                break;
        }
        // Populate the data into the template view using the data object
        if(priceValue != null && !priceValue.equals("")) {
            item.setText(priceModel.getItem());
            price.setText(String.format("\u20B9 %s", priceValue));
        }
        // Return the completed view to render on screen
        return convertView;
    }
}