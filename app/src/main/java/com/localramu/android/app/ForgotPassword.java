package com.localramu.android.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Akram Raza on 25/7/16.
 */
public class ForgotPassword extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        EditText regMobileNumber=(EditText)findViewById(R.id.regMobileNo);
        Button requestOtp=(Button) findViewById(R.id.submit_mobileNo);
        EditText otpRecieved=(EditText)findViewById(R.id.enterOtp);
        Button verifyOtp=(Button)findViewById(R.id.verifyOtp);
        requestOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Please wait...  We are sending your OTP ", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        verifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ForgotPassword.this,VerifyOtp.class);
                startActivity(i);
                Snackbar.make(v, "Please wait...  We are verifing your OTP ", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


    }
}
