package com.localramu.android.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by LENOVO on 8/18/2016.
 */
public class Description extends AppCompatActivity {
    Button bookIt;
    Button seeNearBy;
    ImageView categoryImage;
    TextView categoryDescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bookIt=(Button) findViewById(R.id.bookIt);
        seeNearBy=(Button) findViewById(R.id.seeNearBy);
        categoryImage=(ImageView) findViewById(R.id.beauticianImage);
        categoryDescription=(TextView) findViewById(R.id.description);
        Bundle extras = getIntent().getExtras();
        int position= extras.getInt("position");

         if(position==0){

         }

        else if(position==1){
             categoryDescription.setText(R.string.carpenter);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_carpenter));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",1);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     //Intent intent = new Intent(getBaseContext(),ServicePartnerActivity.class);
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Carpenter");
                     startActivity(intent);
                 }
             });



         }
         else if(position==2){
             categoryDescription.setText(R.string.plumber);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_plumber));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",2);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Plumber");
                     startActivity(intent);
                 }
             });


         }
        else if(position==3){
             categoryDescription.setText(R.string.electrician);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_electrician));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",3);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Electrician");
                     startActivity(intent);
                 }
             });


         }
        else if(position==4){

         }
        else if(position==5){
             categoryDescription.setText(R.string.mechenic);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_mechanic));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",5);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Mechanic");
                     startActivity(intent);
                 }
             });


         }
        else if(position==6){

             categoryDescription.setText(R.string.beautician);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_beauticean));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",6);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Beautician");
                     startActivity(intent);
                 }
             });

        }
        else if(position==7){
             categoryDescription.setText(R.string.dth);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_dth));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",7);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Internet & DTH");
                     startActivity(intent);
                 }
             });

         }
        else if(position==8){
             categoryDescription.setText(R.string.documents);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_documents));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",8);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Documents");
                     startActivity(intent);
                 }
             });


         }
        else if(position==9){
             categoryDescription.setText(R.string.electronics);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_electronics));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",9);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Electronics");
                     startActivity(intent);
                 }
             });


         }
        else if(position==10){
             categoryDescription.setText(R.string.photographer);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_photography));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",10);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Photographer");
                     startActivity(intent);
                 }
             });


         }
        else if(position==11){
             categoryDescription.setText(R.string.tailor);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_tailor));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",11);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Ladies Tailor");
                     startActivity(intent);
                 }
             });

         }
        else if(position==12){
             categoryDescription.setText(R.string.painter);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_painting));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",12);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","House Painting");
                     startActivity(intent);
                 }
             });


         }
        else if(position==13){
             categoryDescription.setText(R.string.puja);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_puja));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",13);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Puja Pandit");
                     startActivity(intent);
                 }
             });


         }
        else if(position==14){
             categoryDescription.setText(R.string.yoga);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_yoga));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",14);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Yoga Teacher");
                     startActivity(intent);
                 }
             });


         }
        else if(position==15){
             categoryDescription.setText(R.string.tanker);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_water_tanker));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",15);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Water Tanker");
                     startActivity(intent);
                 }
             });


         }
        else if(position==16){
             categoryDescription.setText(R.string.packers);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_movers_packer));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",16);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Movers and Packers");
                     startActivity(intent);
                 }
             });


         }
        else if(position==17){
             categoryDescription.setText(R.string.pestcontrol);
             categoryImage.setImageDrawable(getResources().getDrawable(R.drawable.cat_pest_control));
             bookIt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ExpressBookingActivity.class);
                     intent.putExtra("categoryNo",17);
                     startActivity(intent);

                 }
             });
             seeNearBy.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Intent intent= new Intent(Description.this,ServicePartnerActivity.class);
                     intent.putExtra("SELECTED_CATEGORY","Pest Control");
                     startActivity(intent);
                 }
             });

         }
        else{

        }



    }
}
