package com.localramu.android.app;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ListView;
import android.widget.TextView;

import com.localramu.android.app.adapter.AddressArrayAdapter;
import com.localramu.android.app.adapter.LaundaryPriceAdapter;
import com.localramu.android.app.defs.GeneralDefs;
import com.localramu.android.app.models.LaundryPriceModel;
import com.localramu.android.app.utils.AppConfig;
import com.localramu.android.app.utils.Utilities;

import java.util.List;

public class LaundryPriceChartActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private Context mContext;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private List<LaundryPriceModel> priceModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laundry_price_chart);
        Log.e("====1===>","<=1=====");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        AppConfig appConfig = Utilities.getAppConfig(getApplicationContext());
        priceModels = appConfig.getLaundaryPrice();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                getApplicationContext(), priceModels);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }


    public void proceed(View view) {
        Intent intent = new Intent(getApplicationContext(),ExpressBookingActivity.class);
        intent.putExtra("SELECTED_CATEGORY", "Laundry");
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_laundry_price_chart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        private Context mContext;
        private List<LaundryPriceModel> mPriceModels;
        public void setmPriceModels(List<LaundryPriceModel> priceModels) {
            mPriceModels = priceModels;
        }

        public void setmContext(Context context) {
            mContext = context;
        }

        public PlaceholderFragment(){}

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber , Context context,
                                                      List<LaundryPriceModel> priceModels) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            fragment.setmContext(context);
            fragment.setmPriceModels(priceModels);
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_laundry_price_chart, container, false);
            ListView priceChart = (ListView) rootView.findViewById(R.id.price_chart);



            if(mPriceModels != null && mContext != null) {
                LaundaryPriceAdapter adapter = new LaundaryPriceAdapter(mContext, mPriceModels,
                        getArguments().getInt(ARG_SECTION_NUMBER));
                priceChart.setAdapter(adapter);
            }
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        Context mContext;
        List<LaundryPriceModel> mPriceModels;
        public SectionsPagerAdapter(FragmentManager fm, Context context, List<LaundryPriceModel> priceModels) {
            super(fm);
            mContext = context;
            mPriceModels = priceModels;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1, mContext,mPriceModels);
        }

        @Override
        public int getCount() {
            // Show 40dp total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Wash & Iron";
                case 1:
                    return "Wash";
                case 2:
                    return "Iron";
                case 3:
                    return "Dry Wash";
            }
            return null;
        }
    }
}
