package com.localramu.android.app.utils;

import java.util.List;

/**
 * Created by mohitgupta on 20/02/16.
 */
public class TimeUtils {

    public static String convertDaysToString(List<String> daysList) {

        if(daysList == null || daysList.size() == 0 ) {
            return "Mon-Sun";
        }
        if(daysList.size() ==  7) {
            return "Mon-Sun";
        }
        StringBuilder days = new StringBuilder();
        String temp = "";
        for(String day : daysList) {
            if(day.equalsIgnoreCase("Monday")) {
                temp += "Mon,";
            } else if(day.equalsIgnoreCase("Tuesday")) {
                temp += "Tue,";
            } else if(day.equalsIgnoreCase("Wednesday")) {
                temp += "Wed,";
            } else if(day.equalsIgnoreCase("Thursday")) {
                temp += "Thu,";
            } else if(day.equalsIgnoreCase("Friday")) {
                temp += "Fri,";
            }else if(day.equalsIgnoreCase("Saturday")) {
                temp += "Sat,";
            } else if(day.equalsIgnoreCase("Sunday")) {
                temp += "Sun";
            }


        }
        return temp;
    }

    public static String convertTime(String fromTime, String toTime) {
        String convertedTime;
        String fromTimeSplit[]  =  fromTime.split(":");
        if(fromTimeSplit[0].length() == 1) {
            fromTimeSplit[0] = "0" + fromTimeSplit[0];
        }

        if(fromTimeSplit[1].length() == 1) {
            fromTimeSplit[1] = fromTimeSplit[0] + "0";
        }

        String toTimeSplit[]  =  toTime.split(":");
        if(toTimeSplit[0].length() == 1) {
            toTimeSplit[0] = "0" + toTimeSplit[0];
        }

        if(toTimeSplit[1].length() == 1) {
            toTimeSplit[1] = toTimeSplit[0] + "0";
        }

        return String.format("%s:%s - %s:%s", fromTimeSplit[0], fromTimeSplit[1], toTimeSplit[0], toTimeSplit[1]);





    }
}
