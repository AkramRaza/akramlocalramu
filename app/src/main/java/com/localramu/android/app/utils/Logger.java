package com.localramu.android.app.utils;

import android.util.Log;
import com.localramu.android.app.defs.GeneralDefs;

public class Logger {

	public static void z(int priority, String tag, String msg) {

		switch (priority) {
		case 2:
			Log.v(tag, msg);
			break;
		case 3:
			Log.d(tag, msg);
			break;
		case 4:
			Log.i(tag, msg);
			break;
		case 5:
			Log.w(tag, msg);
			break;
		case 6:
			Log.e(tag, msg);
			break;
		case 7:
			System.out.println(tag + msg);
			break;
		}
		if (GeneralDefs.loggingEnabled)
			GeneralDefs.logMessage.append(priority).append('~').append(tag)
					.append('~').append(msg).append('$');

	}

	public static void z(String tag, String msg) {

		Log.d(tag, msg);
		if (GeneralDefs.loggingEnabled)
			GeneralDefs.logMessage.append("$3~").append(tag).append('~')
					.append(msg).append('$');

	}
}
