package com.localramu.android.app.db;


import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.localramu.android.app.defs.TableDefs;
import com.localramu.android.app.utils.Logger;
import com.localramu.common.messages.model.UserInfo;



public class UserInfoDataSource {

	private SQLiteDatabase database;
	private DBHelper dbHelper;
	public UserInfoDataSource(Context context) {
		dbHelper = new DBHelper(context);
	}

	public void write() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void read() throws SQLException {
		database = dbHelper.getReadableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public boolean createUser(UserInfo userInfo, String loginMethod,int isLoggedIn) {
		ContentValues values = new ContentValues();
		values.put(TableDefs.COLUMN_USER_EMAIL, userInfo.getUserId());
		values.put(TableDefs.COLUMN_USER_PASSWORD, userInfo.getPassword());

		values.put(TableDefs.COLUMN_LOGIN_METHOD, loginMethod);
		if(userInfo.getFullName() != null) {
			values.put(TableDefs.COLUMN_FULL_NAME, userInfo.getFullName());
		}
		if(userInfo.getMobileNumber() != null) {
			values.put(TableDefs.COLUMN_MOBILE_NUMBER, userInfo.getMobileNumber());
		}
		if(loginMethod != null) {
			values.put(TableDefs.COLUMN_LOGIN_METHOD, loginMethod);
		}


		values.put(TableDefs.COLUMN_IS_LOGGED_IN,isLoggedIn);
		values.put(TableDefs.COLUMN_IS_GCM_UPDATED,0);
		values.put(TableDefs.COLUMN_LAST_LOG_IN_TIME,String.valueOf(new Date()));

		long id=database.insert(TableDefs.TABLE_USER_INFO, null,values);
		Logger.z("Insertion","Successfully inserted in NickName Table "+userInfo.getUserId() +","+userInfo.getPassword()+","+isLoggedIn);
		if(id==1) {
			Logger.z(4, "UserInfoDataSource", "Created a new user successfully  " + id);
			return true;
		}
		else{
			Logger.z(4,"UserInfoDataSource","User Creation Unsuccessfull "+id);
			return false;
		}
	}


	public void logout() {
		database.delete(TableDefs.TABLE_USER_INFO,TableDefs.COLUMN_IS_LOGGED_IN +"=1",null);
	}

	public void logoutAllUser() {
		ContentValues values = new ContentValues();
		values.put(TableDefs.COLUMN_IS_LOGGED_IN, 0);
		database.update(TableDefs.TABLE_USER_INFO,values,null,null);
	}
	public void loginForExistingUser(UserInfo userInfo, String loginMethod) {
		if(!checkUserExists(userInfo.getUserId())) {
			createUser(userInfo,loginMethod,1);
		} else {
			ContentValues values = new ContentValues();
			values.put(TableDefs.COLUMN_IS_LOGGED_IN, 1);
			database.update(TableDefs.TABLE_USER_INFO,values,TableDefs.COLUMN_USER_EMAIL+"=\""+userInfo.getUserId()+"\"",null);
		}
	}

	public void updatePassword(String password) {
		ContentValues values = new ContentValues();
		values.put(TableDefs.COLUMN_USER_PASSWORD, password);
		database.update(TableDefs.TABLE_USER_INFO,values,TableDefs.COLUMN_IS_LOGGED_IN +"=1",null);

	}
	
	public void updateGcmInfo(String userId, String deviceId, String gcmId, boolean isUpdatedOnServer) {
		ContentValues values = new ContentValues();
		values.put(TableDefs.COLUMN_DEVICE_ID, deviceId);
		values.put(TableDefs.COLUMN_GCM_ID, gcmId);
		values.put(TableDefs.COLUMN_IS_GCM_UPDATED, isUpdatedOnServer ? 1 : 0);
		
		database.update(TableDefs.TABLE_USER_INFO,values,TableDefs.COLUMN_USER_EMAIL +"=\'" + userId+ "\'",null);

	}
	
	


	public String getEmail() {

		Cursor cursor = database.query(TableDefs.TABLE_USER_INFO, new String[] { 
				TableDefs.COLUMN_USER_EMAIL}, TableDefs.COLUMN_IS_LOGGED_IN +"=1",
				null, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();
		else
			return null;
		if (cursor.getCount() > 0){ 

			String userEmail=null;

			userEmail= cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_USER_EMAIL));
			Logger.z(4,"UserInfoDataSource","User Email Fetched - "+userEmail);

			return userEmail;
		}
		else
			return null;

	}


	public UserInfo getUserInfo() {
		UserInfo userInfo = null;
		Cursor cursor = database.query(TableDefs.TABLE_USER_INFO, new String[] {
				TableDefs.COLUMN_USER_EMAIL, TableDefs.COLUMN_DEVICE_ID, TableDefs.COLUMN_GCM_ID, TableDefs.COLUMN_IS_GCM_UPDATED,
				TableDefs.COLUMN_FULL_NAME,TableDefs.COLUMN_MOBILE_NUMBER},
				TableDefs.COLUMN_IS_LOGGED_IN +"=1",
				null, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();
		else
			return null;
		if (cursor.getCount() > 0){
			userInfo = new UserInfo();
			String username=null;
			userInfo.setUserId(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_USER_EMAIL)));
			userInfo.setFullName(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_FULL_NAME)));
			userInfo.setMobileNumber(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_MOBILE_NUMBER)));
			userInfo.setDeviceId(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_DEVICE_ID)));
			userInfo.setGcmId(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_GCM_ID)));
			userInfo.setGcmUpdatedOnServer(cursor.getInt(cursor.getColumnIndex(TableDefs.COLUMN_IS_GCM_UPDATED)) == 1 ? true : false);
			Logger.z(4,"UserInfoDataSource","User Name fetched "+username);
		}
		return userInfo;

	}

	public boolean checkUserExists(String userId) {

		Cursor cursor = database.query(TableDefs.TABLE_USER_INFO, new String[] { 
				TableDefs.COLUMN_USER_EMAIL}, TableDefs.COLUMN_USER_EMAIL +"=\""+userId+"\"",
				null, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();
		else
			return false;
		if (cursor.getCount() > 0){ 
			return true;
		}
		else
			return false;

	}

}
