package com.localramu.android.app;

import android.app.Application;

import com.localramu.common.url.UrlDefs;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

/**
 * Created by mohitgupta on 21/02/16.
 */

@ReportsCrashes(
        formUri = "https://collector.tracepot.com/0d1c4458",
        reportType = HttpSender.Type.JSON,
        httpMethod = HttpSender.Method.POST,
        customReportContent = {
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID,
                ReportField.BUILD,
                ReportField.STACK_TRACE,
                ReportField.DEVICE_ID,
                ReportField.PHONE_MODEL,
                ReportField.DISPLAY,
                ReportField.LOGCAT,
                ReportField.USER_CRASH_DATE
        },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.toast_crash
)

public class ACRAHelper extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // The following line triggers the initialization of ACRA
        ACRA.init(this);
    }
}