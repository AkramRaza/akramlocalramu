package com.localramu.android.app.models;

import android.util.Log;

import java.lang.reflect.Array;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mohitgupta on 05/02/16.
 */
public class DateTimeAdapterData {
    private ArrayList<DateModel> dateList = new ArrayList<DateModel>();
    private ArrayList<TimeRangeModel> timeRangeList = new ArrayList<TimeRangeModel>();

    public static DateTimeAdapterData getRangeInstance() {
        DateTimeAdapterData dateTimeAdapterData = new DateTimeAdapterData();
        dateTimeAdapterData.setDateList();
        dateTimeAdapterData.setTimeRangeList();
        return dateTimeAdapterData;
    }

    private void setDateList() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            dateList.add(new DateModel("Today", calendar.getTime()));
            calendar.add(Calendar.DATE, 1);
            dateList.add(new DateModel("Tomorrow", calendar.getTime()));
            calendar.add(Calendar.DATE, 1);
            for(int i = 0 ; i < 7; i++) {
                dateList.add(new DateModel(dateFormat.format(calendar.getTime()), calendar.getTime()));
                calendar.add(Calendar.DATE,1);

            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void setTimeRangeList() {
        timeRangeList.add(new TimeRangeModel("08:00 - 10:00", "08:00", "10:00",8,10));
        timeRangeList.add(new TimeRangeModel("10:00 - 12:00", "10:00", "12:00",10,12));
        timeRangeList.add(new TimeRangeModel("12:00 - 14:00", "12:00", "14:00",12,14));
        timeRangeList.add(new TimeRangeModel("14:00 - 16:00", "14:00", "16:00",14,16));
        timeRangeList.add(new TimeRangeModel("16:00 - 18:00", "16:00", "18:00", 16, 18));
        timeRangeList.add(new TimeRangeModel("18:00 - 20:00", "18:00", "20:00",18,20));
    }

    public ArrayList<String> getDateStringList() {
        ArrayList<String> dateStringList = new ArrayList<String>();
        for (DateModel date : dateList) {
            dateStringList.add(date.getDateString());
        }
        return dateStringList;
    }

    public ArrayList<String> getTimeStringList() {
        ArrayList<String> timeStringList = new ArrayList<String>();
        for (TimeRangeModel time : timeRangeList) {
            timeStringList.add(time.getTimeString());
        }
        return timeStringList;
    }

    public TimeRangeModel getTimeRange(int position) {
        if(timeRangeList.size() > position) {
            return timeRangeList.get(position);
        }
        return null;
    }

    public DateModel getDate(int position) {
        if(dateList.size() > position) {
            return dateList.get(position);
        }
        return null;
    }

    public int getTimeRangeSize() {
        return timeRangeList.size();
    }

    public int getDateListSize() {
        return dateList.size();
    }
}
