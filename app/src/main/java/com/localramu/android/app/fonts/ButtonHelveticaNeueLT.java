package com.localramu.android.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class ButtonHelveticaNeueLT extends Button{
	  public ButtonHelveticaNeueLT(Context context) {
	        super( context );
	        setFont();

	    }

	    public ButtonHelveticaNeueLT(Context context, AttributeSet attrs) {
	        super( context, attrs );
	        setFont();
	    }

	    public ButtonHelveticaNeueLT(Context context, AttributeSet attrs, int defStyle) {
	        super( context, attrs, defStyle );
	        setFont();
	    }

	    private void setFont() {
	        Typeface normal = Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeueLTPro-Lt.otf");
	        setTypeface( normal, Typeface.NORMAL );

	    }}
