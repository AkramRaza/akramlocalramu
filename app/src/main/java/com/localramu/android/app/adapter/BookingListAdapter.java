package com.localramu.android.app.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.localramu.android.app.BookingDetailsActivity;
import com.localramu.android.app.R;
import com.localramu.android.app.ServiceDetailActivity;
import com.localramu.android.app.utils.JobStatusMapper;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.ExternalJobStatus;
import com.localramu.common.messages.codes.JobStatus;
import com.localramu.common.messages.model.Job;

import java.util.List;

/**
 * Created by mohitgupta on 06/02/16.
 */

public class BookingListAdapter extends RecyclerView.Adapter<BookingListAdapter.ViewHolder> {
    private List<Job> mDataset;
    private Context mContext;
    private Activity mActivity;



    // Provide a suitable constructor (depends on the kind of dataset)
    public BookingListAdapter(List<Job> mDataset, Context context, Activity mActivity) {
        this.mDataset = mDataset;
        this.mContext =  context;
        this.mActivity = mActivity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BookingListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.component_bookings_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        Job job = mDataset.get(position);
        if(job.getAttachedVendorName() != null) {
            holder.fullName.setText(job.getAttachedVendorName());
        } else {
            holder.fullName.setText(job.getCategory());
        }
        if(job.getAttachedVendorDisplayName() != null && !job.getAttachedVendorDisplayName().equals("")) {
            holder.displayName.setText(job.getAttachedVendorDisplayName());
        } else {
            holder.displayName.setText("Awaiting Vendor Confirmation");
        }

        holder.dateTime.setText(job.getJobRequestDate() + " between " + job.getJobRequestTime());

        ExternalJobStatus jobStatus = JobStatusMapper.findExternalJobStatus(job.getJobStatus());
        holder.jobStatus.setText(jobStatus.name());
        if(jobStatus == ExternalJobStatus.COMPLETED) {
           holder.jobStatus.setBackgroundResource(R.drawable.completed_label);
        } else if(jobStatus == ExternalJobStatus.PROCESSING || jobStatus == ExternalJobStatus.ACCEPTED) {
            holder.jobStatus.setBackgroundResource(R.drawable.processing_label);
        } else {
            holder.jobStatus.setBackgroundResource(R.drawable.cancelled_label);
        }
        //holder.distance.setText("4.67 Km Away");
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder{
        protected TextView displayName;
        protected TextView fullName;
        //protected TextView jobDetails;
        protected TextView dateTime;
        protected TextView jobStatus;


        public ViewHolder(View view) {
            super(view);
            this.displayName = (TextView) view.findViewById(R.id.display_name);
            this.fullName = (TextView) view.findViewById(R.id.full_name);
           // this.jobDetails = (TextView) view.findViewById(R.id.job_details);
            this.dateTime = (TextView) view.findViewById(R.id.date_time);
            this.jobStatus = (TextView) view.findViewById(R.id.job_status);
            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    Job job = mDataset.get(pos);
                    Intent intent = new Intent(mContext,BookingDetailsActivity.class);
                    intent.putExtra("JOB_ID", job.getJobId());

                    Bundle bndlanimation =
                            null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        bndlanimation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_in, R.anim.slide_out).toBundle();
                        mActivity.startActivity(intent, bndlanimation);
                    } else {
                        mActivity.startActivity(intent);
                    }

                }
            });
        }


     }



}