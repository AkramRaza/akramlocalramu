package com.localramu.android.app.utils;

import com.localramu.common.messages.codes.ExternalJobStatus;
import com.localramu.common.messages.codes.JobStatus;
import com.localramu.common.messages.codes.PaymentStatus;
import com.localramu.common.messages.enums.JobType;
import com.localramu.common.messages.model.Job;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

/**
 * Created by mohitgupta on 21/02/16.
 */
public class JobFactory {
    public Job buildJob(String category, String customerName, String customerId, String customerAddress,
                        String cusromerPinCode, double latitude, double longitude, String customerMobile,
                        String jobDetails, String requestDate, String requestTime, int priceQuote,
                        String vendorUserId, String serviceId) {

        return createJob(category, customerName, customerId, customerAddress,
                cusromerPinCode, latitude, longitude, customerMobile,
                jobDetails, requestDate, requestTime, priceQuote, vendorUserId,serviceId, null, null);
    }

    public Job buildExpressJob(String category, String customerName, String customerId, String customerAddress,
                               String cusromerPinCode, double latitude, double longitude, String customerMobile,
                               String jobDetails, String requestDate, String requestTime, JobType jobType, Map<String, String> extraJobParams, int priceQuote) {
        return createJob(category, customerName, customerId, customerAddress,
                cusromerPinCode, latitude, longitude, customerMobile,
                jobDetails, requestDate, requestTime, priceQuote, null, null, jobType, extraJobParams);
    }


    private Job createJob(String category, String customerName, String customerId, String customerAddress,
                          String cusromerPinCode, double latitude, double longitude, String customerMobile,
                          String jobDetails, String requestDate, String requestTime, int priceQuote,
                          String vendorUserId, String serviceId, JobType jobType, Map<String, String> extraJobParams) {
        Job job = new Job();
        Date date = new Date();
        job.setJobId(UUID.randomUUID().toString());
        job.setCategory(category);
        job.setCustomerName(customerName);
        job.setCustomerAddress(customerAddress);
        job.setCreationTime(date.getTime());
        job.setCustomerId(customerId);
        job.setCustomerLocationLatitude(latitude);
        job.setCustomerLocationLongitude(longitude);
        job.setCustomerMobile(customerMobile);
        job.setCustomerPinCode(cusromerPinCode);
        job.setJobDetails(jobDetails);
        job.setJobRequestDate(getDate(requestDate));
        job.setJobRequestTime(requestTime);
        job.setPriceQuote(priceQuote);
        job.setJobType(jobType);
        job.setExtraJobInfo(extraJobParams);
        if(vendorUserId != null) {
            job.setAttachedVendorId(vendorUserId);
        }
        if(serviceId != null) {
            job.setAttachedServiceId(serviceId);
        }
        job.setJobStatus(JobStatus.FLIGHT);
        job.setExternalJobStatus(ExternalJobStatus.PROCESSING);
        job.setPaymentStatus(PaymentStatus.PENDING);
        return job;
    }

    private String getDate(String requestedDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        if(requestedDate.equals("Today")) {
            Date date = new Date();
            return String.valueOf(sdf.format(date));
        } else if(requestedDate.equals("Tomorrow")){
            GregorianCalendar gc = new GregorianCalendar();
            gc.add(Calendar.DATE, 1);
            return String.valueOf(sdf.format(gc.getTime()));
        }
        return requestedDate;
    }
}
