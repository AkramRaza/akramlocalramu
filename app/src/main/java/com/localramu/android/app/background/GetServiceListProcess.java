package com.localramu.android.app.background;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.android.app.utils.MD5HashGenerator;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.GetServiceRequestType;
import com.localramu.common.messages.enums.UserType;
import com.localramu.common.messages.model.Service;
import com.localramu.common.messages.service.request.GetServiceListRequest;
import com.localramu.common.messages.service.request.LoginRequest;
import com.localramu.common.messages.service.response.GetServiceListResponse;
import com.localramu.common.messages.service.response.LoginResponse;
import com.localramu.common.url.UrlDefs;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by mohitgupta on 14/02/16.
 */
public class GetServiceListProcess {

    private String category;
    private double latitude, longitude;
    private Context context;
    private UserInfoDataSource userInfoDataSource;

    public GetServiceListProcess(String category, double latitude, double longitude, Context context) {
        this.category = category;
        this.latitude = latitude;
        this.longitude = longitude;
        this.context = context;
        this.userInfoDataSource = new UserInfoDataSource(context);
    }

    public List<Service>  getServices() {
        List<Service> serviceList = null;
        StatusCode statusCode = StatusCode.UNKNOWN;
        try{
            if(category.equals("Mechanic")) {
                category = "Vehicle Maintainance";
            }

            String deviceUserId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            RestTemplate restTemplate = new RestTemplate();
            GetServiceListRequest getServiceListRequest = new GetServiceListRequest();
            getServiceListRequest.setCategory(category);
            getServiceListRequest.setLatitude(latitude);
            getServiceListRequest.setLongitude(longitude);
            getServiceListRequest.setGetServiceRequestType(GetServiceRequestType.LOCATION_AND_CATEGORY);


            restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
            JSONObject json = new JSONObject();
            Gson gson = new Gson();


            Log.i("Request", gson.toJson(getServiceListRequest));
            GetServiceListResponse response = restTemplate.postForObject(UrlDefs.GET_SERVICE_SQL_URL,
                    getServiceListRequest, GetServiceListResponse.class);
            statusCode = response.getStatusCode();
            if(response.isSuccess()) {
                serviceList = response.getServiceList();
                userInfoDataSource.write();
                //userInfoDataSource.logoutAllUser();
                //userInfoDataSource.loginForExistingUser(response.getUserInfo(), "STANDARD");
                userInfoDataSource.close();
            }
            Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());

        }
        catch(Exception e){
            e.printStackTrace();
            statusCode = StatusCode.ERROR_OCCURRED;
        }
        return serviceList;

    }
}
