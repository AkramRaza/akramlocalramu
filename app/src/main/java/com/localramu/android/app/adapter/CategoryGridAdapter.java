package com.localramu.android.app.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.localramu.android.app.R;
import com.localramu.android.app.defs.TableDefs;


/**
 * Created by mohitgupta on 02/02/16.
 */
public class CategoryGridAdapter extends CursorAdapter {
    private Context context;
    private Cursor cursor;
    public CategoryGridAdapter(Context context, Cursor c) {
        super(context, c,CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        this.context = context;
        this.cursor = cursor;
    }


    public void update() {
        this.notifyDataSetChanged();
    }


    @Override
    public void bindView(View view , Context context, Cursor cursor) {

        ViewHolder holder = null;
        if (view == null || view.getTag() == null) {
            holder = new ViewHolder();
            holder.image = (ImageView) view.findViewById(R.id.image);
            holder.text = (TextView) view.findViewById(R.id.text);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.image.setImageResource(cursor.getInt(cursor.getColumnIndex(TableDefs.COLUMN_CATEGORY_IMAGE_ID)));
        holder.text.setText(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_CATEGORY_NAME)));


    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater infl = LayoutInflater.from(context);
        View v = infl.inflate(R.layout.component_category_grid_item, parent, false);
        bindView(v, context, cursor);
        return v;
    }


    private class ViewHolder {
        ImageView image;
        TextView text;
    }
}