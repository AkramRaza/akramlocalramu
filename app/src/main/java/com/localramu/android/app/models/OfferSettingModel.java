package com.localramu.android.app.models;

import com.google.gson.Gson;

/**
 * Created by mohitgupta on 23/04/16.
 */
public class OfferSettingModel {

    private boolean enabled;
    private String primaryText;
    private String secondaryText;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPrimaryText() {
        return primaryText;
    }

    public void setPrimaryText(String primaryText) {
        this.primaryText = primaryText;
    }

    public String getSecondaryText() {
        return secondaryText;
    }

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public static OfferSettingModel fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, OfferSettingModel.class);
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
