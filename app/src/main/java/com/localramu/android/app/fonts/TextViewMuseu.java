package com.localramu.android.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewMuseu extends TextView{
	public TextViewMuseu(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	    init();
	}

	public TextViewMuseu(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    init();
	}

	public TextViewMuseu(Context context) {
	    super(context);
	    init();
	}

	public void init() {

	    Typeface tf = Typeface.createFromAsset
	(getContext().getAssets(), "fonts/Museo/Museo300-Regular.otf");
	    setTypeface(tf ,1);

	}
}
