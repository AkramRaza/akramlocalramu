package com.localramu.android.app;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.localramu.android.app.background.FeedbackProcess;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.model.Job;

public class FeedbackActivity extends AppCompatActivity {


    RatingBar ratingBar;
    TextView rateMessage;
    TextView submitFeedback;
    String ratedValue;
    EditText feedbackMessage;
    String fullMessage;
    public static Context context;
    private Job job;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ratingBar = (RatingBar) findViewById(R.id.feedbackRatingBar);
        rateMessage = (TextView) findViewById(R.id.rateMessage);
        feedbackMessage = (EditText) findViewById(R.id.feedbackMessage);
        submitFeedback = (TextView) findViewById(R.id.submit_feedback);
        fullMessage = feedbackMessage.getText().toString().trim();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratedValue = String.valueOf(ratingBar.getRating());
                rateMessage.setText("You have rated the Product : " + ratedValue + "/5.");
            }
        });
        submitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fullMessage = feedbackMessage.getText().toString().trim();
                ratedValue = String.valueOf(ratingBar.getRating());
                if(fullMessage.equals("")||ratedValue.equals("")){
                    Snackbar.make(getWindow().getDecorView(),"Please fill the feilds",Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }else {
                    sendData();
                }
            }
        });
    }

    public void sendData(){
        new AsyncTask<String, String, StatusCode>() {

            @Override
            protected StatusCode doInBackground(String... params) {


                FeedbackProcess feedbackProcess = new FeedbackProcess(fullMessage, ratedValue,
                        job, getApplicationContext());
                return feedbackProcess.sendFeedback();

            }

            @Override
            protected void onPostExecute(final StatusCode statusCode) {
                super.onPostExecute(statusCode);
                try {
                    if (statusCode == StatusCode.SUCCESS) {
                        Toast.makeText(getApplicationContext(),
                                "Thanks for giving your valuable Feedback", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(FeedbackActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Sorry, Something went wrong!! Please Try again", Toast.LENGTH_LONG).show();
                        /*Intent intent =new Intent(FeedbackActivity.this,DashboardActivity.class);
                        startActivity(intent);*/
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }.execute();
    }
    public void createFeedback(View view) {

    }
}
