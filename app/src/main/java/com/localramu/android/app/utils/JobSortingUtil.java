package com.localramu.android.app.utils;

import com.localramu.common.messages.model.Job;

import java.util.Comparator;

/**
 * Created by mohitgupta on 07/03/16.
 */
public class JobSortingUtil implements Comparator<Job>{

    @Override
    public int compare(Job lhs, Job rhs) {
        return lhs.getCreationTime() < rhs.getCreationTime() ? 1 : -1;
    }
}
