package com.localramu.android.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewThickFont extends TextView{
	public TextViewThickFont(Context context) {
        super(context);
        setFont();

    }

    public TextViewThickFont (Context context, AttributeSet attrs) {
        super( context, attrs );
        setFont();
    }

    public TextViewThickFont (Context context, AttributeSet attrs, int defStyle) {
        super( context, attrs, defStyle );
        setFont();
    }

    private void setFont() {
        Typeface normal = Typeface.createFromAsset(getContext().getAssets(),"fonts/MuseoSansRounded/MuseoSansRounded-500.otf");
        setTypeface( normal, Typeface.NORMAL );

    }
}
