package com.localramu.android.app;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.localramu.android.app.background.JobRequestProcess;
import com.localramu.android.app.background.LoginProcess;
import com.localramu.android.app.db.CategoryDataSource;
import com.localramu.android.app.db.JobsDataSource;
import com.localramu.android.app.defs.GeneralDefs;
import com.localramu.android.app.defs.TableDefs;
import com.localramu.android.app.models.Address;
import com.localramu.android.app.models.DateTimeAdapterData;
import com.localramu.android.app.models.TimeRangeModel;
import com.localramu.android.app.ui.CustomToast;
import com.localramu.android.app.utils.InternetDetector;
import com.localramu.android.app.utils.JobFactory;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.model.Job;
import com.localramu.common.messages.model.UserInfo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class JobBookingActivity extends AppCompatActivity {

    private Spinner dateSpinner, timeRangeSpinner;
    private DateTimeAdapterData dateTimeAdapterData = DateTimeAdapterData.getRangeInstance();

    private String category, vendorUserId, serviceId, visitingCharge, displayName, fullname;
    private Job job;
    private InternetDetector internetDetector;
    static final int ADDRESS_FIND_REQUEST = 1;
    private JobFactory jobFactory = new JobFactory();
    private int dateRangeEnabledIndex = 0, timeRangeEnabledIndex = 0;
    public JobBookingActivity() {
        //categoryDataSource = new CategoryDataSource(getApplicationContext());
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_booking);
        Intent intent = getIntent();
        category = intent.getStringExtra("CATEGORY");
        vendorUserId = intent.getStringExtra("VENDOR_USER_ID");
        serviceId = intent.getStringExtra("SERVICE_ID");
        visitingCharge = intent.getStringExtra("VISITING_CHARGE");
        displayName = intent.getStringExtra("DISPLAY_NAME");
        fullname = intent.getStringExtra("FULL_NAME");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mapViews();
        setAdapter();
        populateData();
        internetDetector = new InternetDetector(getApplicationContext());
    }

    private void mapViews() {
        dateSpinner = (Spinner) findViewById(R.id.date);
        timeRangeSpinner = (Spinner) findViewById(R.id.time_range);

    }
    private void populateData() {
        ((TextView) findViewById(R.id.category)).setText(category);
        ((TextView) findViewById(R.id.display_name)).setText(displayName);
        ((TextView) findViewById(R.id.full_name)).setText(fullname);
        ((TextView) findViewById(R.id.average_cost)).setText(visitingCharge);
        EditText locality = (EditText) findViewById(R.id.locality);
        Address address = Utilities.getSelectedAddress(getApplicationContext());
        if(address != null) {
            locality.setText(address.getAddressSecondary());
            locality.setEnabled(false);
            if(address.getMobileNumber() != null) {
                ((EditText) findViewById(R.id.address)).setText(address.getAddressPrimary());
                ((EditText) findViewById(R.id.landmark)).setText(address.getLandmark());
                ((EditText) findViewById(R.id.mobile)).setText(address.getMobileNumber());
            }
        }


    }

    private void setAdapter() {
        ArrayAdapter<String> dateArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, dateTimeAdapterData.getDateStringList()) {
            // Disable click item < month current
            @Override
            public boolean isEnabled(int position) {
                int currentHour = getCurrentHour();
                if (position == 0 && !isValidBookingTimeForToday()) {
                    dateRangeEnabledIndex = 1;
                    return false;
                }
                return true;
            }
            // Change color item
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                // TODO Auto-generated method stub
                View mView = super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                int currentHour = getCurrentHour();
                if (position == 0 && !isValidBookingTimeForToday()) {
                    mTextView.setTextColor(Color.GRAY);

                }
                return mView;
            }
        };
        dateArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateSpinner.setAdapter(dateArrayAdapter);


        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                int item = dateSpinner.getSelectedItemPosition();
                timeRangeSpinner.setAdapter(null);
                timeRangeSpinner.invalidate();
                if (item == 0) {
                    timeRangeSpinner.setAdapter(setTimeRangeAdapter(true));
                } else {
                    timeRangeSpinner.setAdapter(setTimeRangeAdapter(false));
                }
                //timeRangeSpinner.setSelection(timeRangeEnabledIndex);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        timeRangeSpinner.setAdapter(setTimeRangeAdapter(true));



    }

    private ArrayAdapter<String> setTimeRangeAdapter(final boolean isTodaysDate) {
        timeRangeEnabledIndex = 0;
        ArrayAdapter<String> arrayAdapter =  new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, dateTimeAdapterData.getTimeStringList()) {
            // Disable click item < month current
            @Override
            public boolean isEnabled(int position) {
                int currentHour = getCurrentHour();
                TimeRangeModel timeRangeModel = dateTimeAdapterData.getTimeRange(position);
                if (timeRangeModel != null && isTodaysDate && currentHour > timeRangeModel.getFromTimeInt()) {
                    timeRangeEnabledIndex = position + 1;
                    return false;
                }
                return true;
            }
            // Change color item
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                // TODO Auto-generated method stub
                View mView = super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                int currentHour = getCurrentHour();
                TimeRangeModel timeRangeModel = dateTimeAdapterData.getTimeRange(position);
                if (timeRangeModel != null && isTodaysDate && currentHour > timeRangeModel.getFromTimeInt()) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return arrayAdapter;
    }

    private boolean validateData() {
        boolean isDataValid = true;
        EditText message = (EditText) findViewById(R.id.message);
        if(message.getText().toString().equals("")) {
            message.setError("Please enter what you need in service");
            isDataValid = false;
        }

        EditText address = (EditText) findViewById(R.id.address);
        if(address.getText().toString().equals("")) {
            address.setError("Please enter where you need the service");
            isDataValid = false;
        }

        EditText locality = (EditText) findViewById(R.id.locality);
        if(locality.getText().toString().equals("")) {
            locality.setError("Please enter locality information");
            isDataValid = false;
        }

        EditText mobile = (EditText) findViewById(R.id.mobile);
        if(mobile.getText().toString().equals("")) {
            mobile.setError("Please enter your mobile number");
            isDataValid = false;
        }

        return isDataValid;
    }

    public void createJobRequest(View view) {
        if(!internetDetector.isConnectingToInternet()) {
            CustomToast.makeToast(3, getApplicationContext(), "No Internet. Please check the network setting", this);
            return;
        }
        if(validateData()) {
            String jobDetails = ((EditText) findViewById(R.id.message)).getText().toString();
            String address = ((EditText) findViewById(R.id.address)).getText().toString();
            String locality = ((EditText) findViewById(R.id.locality)).getText().toString();
            String landmark = ((EditText) findViewById(R.id.landmark)).getText().toString();
            String mobile = ((EditText) findViewById(R.id.mobile)).getText().toString();
            String requestDate = ((Spinner) findViewById(R.id.date)).getSelectedItem().toString();
            String requestTime = ((Spinner) findViewById(R.id.time_range)).getSelectedItem().toString();

            UserInfo userInfo = Utilities.getUserInfo(getApplicationContext());
            if(userInfo == null) {
                userInfo = new UserInfo();
                //Intent intent = new Intent(this, UserLoginActivity.class);
                //startActivity(intent);
            }

            Address selectedAddress = Utilities.getSelectedAddress(getApplicationContext());
            if(selectedAddress == null) {
                Intent intent = new Intent(this, AddressSearchActivity.class);
                startActivityForResult(intent, ADDRESS_FIND_REQUEST);
                return;
            } else {
                selectedAddress.setAddressPrimary(address);
                selectedAddress.setLandmark(landmark);
                selectedAddress.setMobileNumber(mobile);
                Utilities.updateSavedAddress(selectedAddress, getApplicationContext());
            }


            Job job = jobFactory.buildJob(category, null, userInfo.getUserId(),
                    String.format("%s, %s, Landmark - %s", address,locality,landmark),
                    null, selectedAddress.getLatitude(), selectedAddress.getLongitude(), mobile,
                    jobDetails, requestDate, requestTime, Integer.valueOf(visitingCharge), vendorUserId,serviceId);

            new JobRequestTask(job).execute();

        }
    }

    /**
     * Represents an asynchronous Job Creation Request task used tocreate new job
     * the user.
     */
    public class JobRequestTask extends AsyncTask<Void, Void, StatusCode> {

        private final Job mJob;

        JobRequestTask(Job job) {
            mJob = job;
        }

        @Override
        protected void onPreExecute() {
            showProgress();
        }

        @Override
        protected StatusCode doInBackground(Void... params) {

            JobRequestProcess jobRequestProcess = new JobRequestProcess(mJob,getApplicationContext());
            StatusCode statusCode = jobRequestProcess.createJob();
            return statusCode;

        }

        @Override
        protected void onPostExecute(final StatusCode statusCode) {
            if(statusCode == StatusCode.SUCCESS) {
                Intent intent = new Intent(getApplicationContext(),BookingsActivity.class);
                startActivity(intent);
                //registerGcm();
                finish();
            }
            else {
                CustomToast.makeToast(3, JobBookingActivity.this.getApplicationContext(), "Unable to create request at this time , please try in sometime",
                        JobBookingActivity.this);

            }
            cancelProgress();
        }

        @Override
        protected void onCancelled() {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == ADDRESS_FIND_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String result=data.getStringExtra("result");
                if(result != null) {
                    createJobRequest(null);
                }
            }
        }
    }

    private void showProgress() {
        ((RelativeLayout)findViewById(R.id.job_request_button)).setEnabled(false);
        ((TextView) findViewById(R.id.job_request_button_text)).setVisibility(View.GONE);
        ((ProgressBar) findViewById(R.id.job_request_button_progress)).setVisibility(View.VISIBLE);
    }

    private void cancelProgress() {
        ((RelativeLayout)findViewById(R.id.job_request_button)).setEnabled(true);
        ((TextView) findViewById(R.id.job_request_button_text)).setVisibility(View.VISIBLE);
        ((ProgressBar) findViewById(R.id.job_request_button_progress)).setVisibility(View.GONE);
    }

    private int getCurrentHour() {
        Calendar calender = Calendar.getInstance();
        calender.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        Log.i("Current Time in IST", "" + calender.get(Calendar.HOUR_OF_DAY));
        return calender.get(Calendar.HOUR_OF_DAY);

    }

    private boolean isValidBookingTimeForToday() {
        int currentHour = getCurrentHour();
        return (currentHour >= 8 && currentHour <= 19);
    }
}
