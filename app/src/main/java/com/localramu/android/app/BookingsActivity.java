package com.localramu.android.app;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.localramu.android.app.adapter.BookingListAdapter;
import com.localramu.android.app.adapter.ServicePartnerListAdapter;
import com.localramu.android.app.background.FeedbackProcess;
import com.localramu.android.app.background.GetJobListProcess;
import com.localramu.android.app.background.GetServiceListProcess;
import com.localramu.android.app.ui.RecyclerItemClickListener;
import com.localramu.android.app.utils.Constants;
import com.localramu.android.app.utils.JobSortingUtil;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.JobStatus;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.model.Job;
import com.localramu.common.messages.model.Service;
import com.localramu.common.messages.model.UserInfo;
import com.localramu.common.messages.service.request.FeedbackRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class BookingsActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String category;
    private ProgressDialog progressDialog;
    private ObjectAnimator animation;
    private SharedPreferences sharedpreferences;
    private List<String> jobProcessList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mapResource();
        sharedpreferences = getSharedPreferences(Constants.LOCAL_RAMU, Context.MODE_PRIVATE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetServiceListTask().execute();
    }

    private void mapResource () {
        mRecyclerView = (RecyclerView) findViewById(R.id.service_detail_list);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


    }

    /**
     * Represents an asynchronous to get the list of Services
     */
    public class GetServiceListTask extends AsyncTask<Void, Void, List<Job>> {


        @Override
        protected void onPreExecute() { showProgress(); }
        @Override
        protected List<Job> doInBackground(Void... params) {
            UserInfo userInfo = Utilities.getUserInfo(getApplicationContext());
            if(userInfo != null) {
                GetJobListProcess getJobListProcess = new GetJobListProcess(userInfo.getUserId(),
                        getApplicationContext());
                return getJobListProcess.getJobs();
            } else {

                return null;
            }
        }


        @Override
        protected void onPostExecute(final List<Job> jobList) {
            String storedList = sharedpreferences.getString(Constants.BOOKING_HISTORY, null);
            if(storedList != null && !storedList.equals("null")) {
                jobProcessList = new Gson().fromJson(storedList, List.class);
            }
            for(final Job job : jobList)
            {
                if(job.getJobStatus().toString().equalsIgnoreCase(JobStatus.COMPLETED.toString()))
                {

                   if(jobProcessList != null) {
                       Iterator<String> iter = jobProcessList.iterator();
                       while(iter.hasNext()){
                           String jobStoredId = iter.next();
                           if (jobStoredId.equals(job.getJobId())) {
                               final Dialog dialog = new Dialog(BookingsActivity.this);

                               dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                               dialog.setContentView(R.layout.feedback_vendor);
                               dialog.setCanceledOnTouchOutside(false);
                               TextView orderName = (TextView) dialog.findViewById(R.id.order_name);
                               final EditText feedbackMessege = (EditText) dialog.findViewById(R.id.feedbackMessege);
                               final RatingBar feedbackServiceRatingBar = (RatingBar) dialog.findViewById(R.id.feedbackServiceRatingBar);
                               orderName.setText("Order Name : " + job.getJobDetails());
                               Button ok = (Button) dialog.findViewById(R.id.ok);
                               ok.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       sendFeedback(job, feedbackMessege.getText().toString(), "" + feedbackServiceRatingBar.getRating());
                                       dialog.dismiss();
                                   }
                               });
                               jobProcessList.remove(jobStoredId);
                               dialog.show();
                           }
                       }
                   }


                }
                if(job.getJobStatus().toString().equalsIgnoreCase(JobStatus.FLIGHT.toString()) && !jobProcessList.contains(job.getJobId()))
                {
                    jobProcessList.add(job.getJobId());
                }

            }
            String jobListStr = new Gson().toJson(jobProcessList);

            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(Constants.BOOKING_HISTORY, jobListStr);
            editor.commit();
            if(jobList != null && jobList.size() != 0) {
                // specify an adapter (see also next example)
                Collections.sort(jobList, new JobSortingUtil());
                mAdapter = new BookingListAdapter(jobList,getApplicationContext(), BookingsActivity.this);
                mRecyclerView.setAdapter(mAdapter);
            } else {
                BookingsActivity.this.findViewById(R.id.no_service_banner).setVisibility(View.VISIBLE);
            }
            cancelProgress();
        }

        @Override
        protected void onCancelled() {
            cancelProgress();
        }
    }

    public void sendFeedback(final Job job, final String feedback, final String rating)
    {
        String deviceUserId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        FeedbackRequest feedbackRequest  =new FeedbackRequest();
        feedbackRequest.setJobId(job.getJobId());
        feedbackRequest.setRequestId(job.getCustomerId());
        feedbackRequest.setRequestingDevice(deviceUserId);
        feedbackRequest.setFeedback(feedback);
        feedbackRequest.setRating(rating);

        new AsyncTask<String, String, StatusCode>() {

            @Override
            protected StatusCode doInBackground(String... params) {


                FeedbackProcess feedbackProcess = new FeedbackProcess(feedback, rating, job,
                        getApplicationContext());
                return feedbackProcess.sendFeedback();

            }

            @Override
            protected void onPostExecute(final StatusCode statusCode) {
                super.onPostExecute(statusCode);
                try {
                    if (statusCode == StatusCode.SUCCESS) {
                        Toast.makeText(getApplicationContext(),
                                "Thanks for giving your valuable Feedback", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(BookingsActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Sorry, Something went wrong!! Please Try again", Toast.LENGTH_LONG).show();
                        /*Intent intent =new Intent(FeedbackActivity.this,DashboardActivity.class);
                        startActivity(intent);*/
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }.execute();
    }




    private void showProgress() {
        // Locate view
        TextView ring = (TextView) findViewById(R.id.ring);

        animation = ObjectAnimator.ofFloat(ring, "rotationY", 0.0f, 360f);
        animation.setDuration(2000);
        animation.setRepeatCount(ObjectAnimator.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        animation.start();
    }

    private void cancelProgress() {
        if(animation != null) {
            animation.end();
        }
        ((RelativeLayout) findViewById(R.id.custom_progress)).setVisibility(View.GONE);
    }
}
