package com.localramu.android.app.background;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.JobsDataSource;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.GetJobRequestType;
import com.localramu.common.messages.enums.GetServiceRequestType;
import com.localramu.common.messages.model.Job;
import com.localramu.common.messages.model.Service;
import com.localramu.common.messages.service.request.GetJobRequest;
import com.localramu.common.messages.service.request.GetServiceListRequest;
import com.localramu.common.messages.service.response.GetJobResponse;
import com.localramu.common.messages.service.response.GetServiceListResponse;
import com.localramu.common.url.UrlDefs;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by mohitgupta on 14/02/16.
 */
public class GetJobListProcess {

    private String mCustomerId;
    private Context mContext;
    private JobsDataSource mJobsDataSource;

    public GetJobListProcess(String customerId, Context context) {
        mCustomerId = customerId;
        mContext = context;
        mJobsDataSource = new JobsDataSource(context);
    }

    public List<Job>  getJobs() {
        List<Job> jobList = null;
        try{

            String deviceUserId = Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            RestTemplate restTemplate = new RestTemplate();
            GetJobRequest getJobRequest = new GetJobRequest();
            getJobRequest.setCustomerId(mCustomerId);
            getJobRequest.setGetJobRequestType(GetJobRequestType.BY_CUSTOMER_ID);


            restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
            JSONObject json = new JSONObject();
            Gson gson = new Gson();


            Log.i("Request", gson.toJson(getJobRequest));
            GetJobResponse response = restTemplate.postForObject(UrlDefs.GET_JOB_URL,
                    getJobRequest, GetJobResponse.class);
            if(response.isSuccess()) {
                jobList = response.getJobList();
                mJobsDataSource.write();
                mJobsDataSource.updateJobList(jobList);
                mJobsDataSource.close();
            }
            Log.i("Httppost created :",response.getStatusCode()+","+gson.toJson(response));

        }
        catch(Exception e){
            e.printStackTrace();
        }
        return jobList;

    }
}
