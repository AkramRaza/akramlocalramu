package com.localramu.android.app.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.localramu.android.app.R;
import com.localramu.android.app.ServiceDetailActivity;
import com.localramu.android.app.models.Address;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.model.Service;
import com.localramu.android.app.utils.DistanceCalculator;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by mohitgupta on 06/02/16.
 */

public class ServicePartnerListAdapter extends RecyclerView.Adapter<ServicePartnerListAdapter.ViewHolder> {
    private List<Service> mDataset;
    private Context mContext;
    private Activity mActivity;
    private DistanceCalculator distanceCalculator = new DistanceCalculator();




    // Provide a suitable constructor (depends on the kind of dataset)
    public ServicePartnerListAdapter(List<Service> mDataset, Context context, Activity mActivity) {
        this.mDataset = mDataset;
        this.mContext =  context;
        this.mActivity = mActivity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ServicePartnerListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.component_service_partner_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        Service service = mDataset.get(position);
        String category = service.getServiceInfo().getCategory();
        int resourceId = Utilities.getImageResource(category, mContext);

        if(resourceId == 0) {
           resourceId = R.drawable.logo;
        }
        holder.imageView.setImageResource(resourceId);
        holder.displayName.setText(service.getBasicInfo().getDisplayName());
        holder.category.setText(service.getServiceInfo().getCategory());
        holder.experience.setText(service.getServiceInfo().getServiceExperience());
        if(service.getServiceInfo().getVisitingCost() != null ) {
            holder.averageVisitingCost.setText("\u20B9  " + service.getServiceInfo().getVisitingCost());
        } else {
            holder.averageVisitingCost.setText("\u20B9  100");

        }
        holder.rating.setText(service.getRating());
        Address address = Utilities.getSelectedAddress(mContext);

        if(address != null) {
            double distance = distanceCalculator.distance(address.getLatitude(), address.getLongitude(), service.getLatitude(),
                    service.getLongitude(),"K");
            holder.distance.setText(String.format("%.2f Km Away", distance));
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder{
        protected ImageView imageView;
        protected TextView displayName;
        protected TextView category;
        protected TextView averageVisitingCost;
        protected TextView rating;
        protected TextView distance;
         protected TextView experience;


        public ViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.service_partner_image);
            this.displayName = (TextView) view.findViewById(R.id.display_name);
            this.category = (TextView) view.findViewById(R.id.category);
            this.averageVisitingCost = (TextView) view.findViewById(R.id.average_cost);
            this.rating = (TextView) view.findViewById(R.id.rating);
            this.distance = (TextView) view.findViewById(R.id.distance);
            this.experience = (TextView) view.findViewById(R.id.service_experience);
            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    Service service = mDataset.get(pos);
                    Intent intent = new Intent(mContext,ServiceDetailActivity.class);
                    intent.putExtra("SERVICE_ID", service.getServiceId());
                    intent.putExtra("DISPLAY_NAME", service.getBasicInfo().getDisplayName());
                    intent.putExtra("VENDOR_ID", service.getVendorUserId());

                    Bundle bndlanimation =
                            null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        bndlanimation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_in, R.anim.slide_out).toBundle();
                        mActivity.startActivity(intent, bndlanimation);
                    } else {
                        mActivity.startActivity(intent);
                    }

                }
            });
        }


     }



}