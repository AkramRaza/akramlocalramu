package com.localramu.android.app.defs;

public class TableDefs {

	public static final String DATABASE_NAME = "scatter_db";
	public static final int DATA_VERSION = 25;
	
	public static final String COLUMN_ID = "_id";
	
	public static final String TABLE_USER_INFO = "userInfo";
	public static final String COLUMN_USER_EMAIL = "userEmail";
	public static final String COLUMN_USER_PASSWORD = "userPassword";
	public static final String COLUMN_IS_LOGGED_IN = "isLoggedIn";
    public static final String COLUMN_FULL_NAME = "fullName";
	public static final String COLUMN_LOGIN_METHOD = "loginMethod";
	public static final String COLUMN_IMAGE_URL = "imageUrl";
	public static final String COLUMN_MOBILE_NUMBER = "mobileNumber";
	public static final String COLUMN_GCM_ID = "gcmId";
	public static final String COLUMN_DEVICE_ID = "deviceId";
	public static final String COLUMN_LAST_LOG_IN_TIME = "lastLogInTime";
	public static final String COLUMN_IS_GCM_UPDATED = "GcmIdUpdated";

	public static final String TABLE_CATEGORY = "category";
	public static final String COLUMN_CATEGORY_NAME = "categoryName";
	public static final String COLUMN_CATEGORY_DETAILS = "categoryDetails";
	public static final String COLUMN_CATEGORY_IMAGE_ID = "catImageId";
	public static final String COLUMN_MENU_PATH_NORMAL = "menuPathNormal";
	public static final String COLUMN_NEXT_ACTIVITY = "nextActivity";
	public static final String COLUMN_IS_CATEGORY_ENABLED = "IsCategoryEnabled";
	public static final String COLUMN_IS_DESCRIPTION = "Description";

	public static final String TABLE_SERVICE = "service";
	public static final String COLUMN_SERVICE_SERVICE_ID = "serviceId";
	public static final String COLUMN_SERVICE_DETAILS = "serviceDetails";
	public static final String COLUMN_IS_DRAFT = "isDraft";
	public static final String COLUMN_CREATION_TIME = "creationTime";
	public static final String COLUMN_LAST_UPDATE_TIME = "lastUpdateTime";
	
	public static final String TABLE_ADDRESS = "address";
    public static final String ADDRESS_ID = "addressId";
    public static final String COLUMN_ADDRESS_TYPE = "addressType";
    public static final String COLUMN_ADDRESS_PRIMARY = "addressPrimary";
    public static final String COLUMN_ADDRESS_SECONDARY = "addressSecondary";
    public static final String COLUMN_LOCALITY = "locality";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
	public static final String COLUMN_LANDMARK = "landmark";

	public static final String TABLE_JOB = "job";
	public static final String COLUMN_JOB_ID = "jobId";
	public static final String COLUMN_JOB_DETAILS = "jobDetails";
	public static final String COLUMN_JOB_STATUS = "jobStatus";

				
	
}
