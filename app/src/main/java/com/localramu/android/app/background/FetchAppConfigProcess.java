package com.localramu.android.app.background;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.android.app.defs.GeneralDefs;
import com.localramu.android.app.utils.AppConfig;
import com.localramu.common.messages.codes.AppConfigClient;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.UserInfoUpdateType;
import com.localramu.common.messages.model.UserInfo;
import com.localramu.common.messages.service.request.GetAppConfigRequest;
import com.localramu.common.messages.service.request.UpdateUserInfoRequest;
import com.localramu.common.messages.service.response.GetAppConfigResponse;
import com.localramu.common.messages.service.response.UpdateUserInforResponse;
import com.localramu.common.url.UrlDefs;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.FileOutputStream;

/**
 * Created by mohitgupta on 23/04/16.
 */
public class FetchAppConfigProcess {
    private Context context;

    public FetchAppConfigProcess(Context context) {
        this.context = context;
    }

    public StatusCode updateToken() {
        StatusCode statusCode = StatusCode.UNKNOWN;;
        try{
            String deviceUserId = Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);

            RestTemplate restTemplate = new RestTemplate();
            GetAppConfigRequest getAppConfigRequest = new GetAppConfigRequest();
            getAppConfigRequest.setRequestingDevice(deviceUserId);
            getAppConfigRequest.setAppConfigClient(AppConfigClient.CUSTOMER_APP);

            restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
            Gson gson = new Gson();

            Log.i("Request", gson.toJson(getAppConfigRequest));
            GetAppConfigResponse response = restTemplate.postForObject(UrlDefs.GET_APP_CONFIG_URL,
                    getAppConfigRequest, GetAppConfigResponse.class);
            if(response.isSuccess()) {
                statusCode = response.getStatusCode();
                String filename = GeneralDefs.APP_CONFIG_FILE;
                String config = response.getConfig();
                FileOutputStream outputStream;
                try {
                    outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
                    outputStream.write(config.getBytes());
                    outputStream.close();
                    GeneralDefs.appConfig = new AppConfig(config);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                statusCode = StatusCode.FAILURE;
            }
            Log.i("Httppost created :", response.getStatusCode() + "," + response.isSuccess());

        }
        catch(Exception e){
            e.printStackTrace();
            statusCode = StatusCode.ERROR_OCCURRED;
        }
        return statusCode;

    }
}
