package com.localramu.android.app;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.localramu.android.app.background.FetchAppConfigProcess;
import com.localramu.android.app.defs.SharedPreferencesKeys;
import com.localramu.android.app.gcm.GcmRegistrationIntentService;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.model.UserInfo;

public class LoadingConfigActivity extends AppCompatActivity {

    private ObjectAnimator animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        showProgress();
        new LoadConfigProcess().execute();

    }

    private void showProgress() {
        // Locate view
        TextView ring = (TextView) findViewById(R.id.ring);

        animation = ObjectAnimator.ofFloat(ring, "rotationY", 0.0f, 360f);
        animation.setDuration(2000);
        animation.setRepeatCount(ObjectAnimator.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        animation.start();
    }

    private void cancelProgress() {
        if(animation != null) {
            animation.end();
        }
        ((RelativeLayout) findViewById(R.id.custom_progress)).setVisibility(View.GONE);
    }

    private class LoadConfigProcess extends AsyncTask<Void, Void, StatusCode> {

        @Override
        protected StatusCode doInBackground(Void... params) {
            FetchAppConfigProcess fetchAppConfigProcess = new FetchAppConfigProcess(getApplicationContext());
            return fetchAppConfigProcess.updateToken();
        }

        @Override
        protected void onPostExecute(StatusCode statusCode) {

            if(statusCode != null && statusCode == StatusCode.SUCCESS) {
                Intent intent = new Intent(getBaseContext(),DashboardActivity.class);
                startActivity(intent);
                LoadingConfigActivity.this.finish();
            } else {

            }

        }

    }

}
