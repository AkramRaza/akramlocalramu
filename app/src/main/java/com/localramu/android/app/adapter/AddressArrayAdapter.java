package com.localramu.android.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.localramu.android.app.R;
import com.localramu.android.app.models.Address;

import java.util.List;

/**
 * Created by mohitgupta on 17/02/16.
 */
public class AddressArrayAdapter extends ArrayAdapter<Address> {

    public AddressArrayAdapter(Context context, List<Address> values) {
        super(context, 0,values);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Address address = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.component_address_list_item, parent, false);
        }
        // Lookup view for data population
        TextView mAddress = (TextView) convertView.findViewById(R.id.address);

        StringBuilder addressBuilder = new StringBuilder(address.getAddressPrimary());
        addressBuilder.append(",");
        addressBuilder.append(address.getAddressSecondary());
        addressBuilder.append(",");
        if(address.getLandmark() != null) {
            addressBuilder.append(", Landmark - ");
            addressBuilder.append(address.getLandmark());

        }
        if(address.getMobileNumber() != null) {
            addressBuilder.append(", Mobile - ");
            addressBuilder.append(address.getMobileNumber());
        }
        // Populate the data into the template view using the data object
        mAddress.setText(addressBuilder.toString());

        // Return the completed view to render on screen
        return convertView;
    }
}