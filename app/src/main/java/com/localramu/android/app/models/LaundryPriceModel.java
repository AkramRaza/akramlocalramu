package com.localramu.android.app.models;

import com.google.gson.Gson;

/**
 * Created by mohitgupta on 06/03/16.
 */
public class LaundryPriceModel {

    private String item;
    private String price;
    private int  imageResource;
    private String washAndIron;
    private String washPrice;
    private String ironPrice;
    private String dryWashPrice;

    public LaundryPriceModel(String item, String price, int imageResource) {
        this.item = item;
        this.price = price;
        this.imageResource = imageResource;
    }

    public LaundryPriceModel(String item, String price) {
        this.item = item;
        this.price = price;

    }



    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getWashAndIron() {
        return washAndIron;
    }

    public void setWashAndIron(String washAndIron) {
        this.washAndIron = washAndIron;
    }

    public String getWashPrice() {
        return washPrice;
    }

    public void setWashPrice(String washPrice) {
        this.washPrice = washPrice;
    }

    public String getIronPrice() {
        return ironPrice;
    }

    public void setIronPrice(String ironPrice) {
        this.ironPrice = ironPrice;
    }

    public String getDryWashPrice() {
        return dryWashPrice;
    }

    public void setDryWashPrice(String dryWashPrice) {
        this.dryWashPrice = dryWashPrice;
    }

    public static LaundryPriceModel fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, LaundryPriceModel.class);
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
