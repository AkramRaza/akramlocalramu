package com.localramu.android.app.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.localramu.android.app.R;
import com.localramu.android.app.defs.TableDefs;
import com.localramu.android.app.utils.Logger;
import com.localramu.common.messages.model.UserInfo;


import java.sql.SQLException;

/**
 * Created by mohitgupta on 02/02/16.
 */
public class CategoryDataSource {


    private SQLiteDatabase database;
    private DBHelper dbHelper;
    Context context;
    private String[] allColumns = {TableDefs.COLUMN_ID, TableDefs.COLUMN_CATEGORY_NAME,TableDefs.COLUMN_CATEGORY_DETAILS,
            TableDefs.COLUMN_CATEGORY_IMAGE_ID,TableDefs.COLUMN_IS_CATEGORY_ENABLED, TableDefs.COLUMN_MENU_PATH_NORMAL, TableDefs.COLUMN_IS_DESCRIPTION};
    //private String catArray[]
    public CategoryDataSource(Context context) {
        dbHelper = new DBHelper(context);
        this.context = context;
    }

    public void write() throws SQLException {
        database = dbHelper.getReadableDatabase();
    }

    public void read() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    public boolean init()
    {
        if(hasCategories() == 18)
            return true;
        else
        {
            createCategory("Water Can", null, R.drawable.cat_water, 1, 0,context.getResources().getString(R.string.beautician));
            createCategory("Carpenter", null, R.drawable.cat_carpenter, 1 , 1,context.getResources().getString(R.string.carpenter));
            createCategory("Plumber", null, R.drawable.cat_plumber, 1, 1,context.getResources().getString(R.string.plumber));
            createCategory("Electrician", null, R.drawable.cat_electrician, 1, 1,context.getResources().getString(R.string.electrician));
            createCategory("Laundry", null, R.drawable.cat_laundry, 1, 0,context.getResources().getString(R.string.beautician));
            createCategory("Mechanic", null, R.drawable.cat_mechanic, 1, 1,context.getResources().getString(R.string.mechenic));
            createCategory("Beautician", null, R.drawable.cat_beauticean, 1, 1,context.getResources().getString(R.string.beautician));
            createCategory("Internet & DTH", null, R.drawable.cat_dth, 1, 1,context.getResources().getString(R.string.dth));
            createCategory("Documents", null, R.drawable.cat_documents, 1, 1,context.getResources().getString(R.string.documents));
            createCategory("Electronics", null, R.drawable.cat_electronics, 1, 1,context.getResources().getString(R.string.electronics));
            createCategory("Photographer", null, R.drawable.cat_photography, 1, 1,context.getResources().getString(R.string.photographer));
            createCategory("Ladies Tailor", null, R.drawable.cat_tailor, 1, 1,context.getResources().getString(R.string.tailor));
            createCategory("House Painting", null,R.drawable.cat_painting, 1, 1,context.getResources().getString(R.string.painter)) ;
            createCategory("Puja Pandit", null,R.drawable.cat_puja, 1, 1,context.getResources().getString(R.string.puja));
            createCategory("Yoga Teacher", null,R.drawable.cat_yoga, 1, 1,context.getResources().getString(R.string.yoga));
            createCategory("Water Tanker", null,R.drawable.cat_water_tanker, 1, 1,context.getResources().getString(R.string.tanker));
            createCategory("Movers and Packers", null,R.drawable.cat_movers_packer, 1, 1,context.getResources().getString(R.string.packers));
            createCategory("Pest Control", null,R.drawable.cat_pest_control, 1, 1,context.getResources().getString(R.string.pestcontrol));

        }
        return true;

    }

    public void createCategory(String category, String categoryDetails, int categoryImageResource,int isEnabled, int isMenuNormal, String description) {
        ContentValues values = new ContentValues();

        values.put(TableDefs.COLUMN_CATEGORY_NAME,category);
        values.put(TableDefs.COLUMN_CATEGORY_IMAGE_ID,categoryImageResource);
        values.put(TableDefs.COLUMN_IS_CATEGORY_ENABLED,isEnabled);
        values.put(TableDefs.COLUMN_MENU_PATH_NORMAL,isMenuNormal);
        values.put(TableDefs.COLUMN_IS_DESCRIPTION,description);

        database.insert(TableDefs.TABLE_CATEGORY, null, values);

    }


    public int hasCategories()
    {
        int count;
        Cursor cursor = database.query(TableDefs.TABLE_CATEGORY, allColumns,null,
                null, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();


        count  = cursor.getCount();
        cursor.close();
        return count;

    }

    public Cursor getCategoryList()
    {
        Cursor cursor =null;

        cursor  = database.query(TableDefs.TABLE_CATEGORY,
                allColumns,  null, null, null, null, null);
        //new String[]{TableDefs.COLUMN_PARENT_AUTHOR,TableDefs.COLUMN_MESSAGE_ID,TableDefs.COLUMN_PARENT_MESSAGE,TableDefs.COLUMN_READ_STATUS}, TableDefs.COLUMN_USER_EMAIL +"=\""+GeneralVariables.userEmail+"\"",null, null,null, null);
        if(cursor != null)
            cursor.moveToFirst();
        Log.i("CategoryListDataSource", "No of categories " + cursor.getCount());


        return cursor;
    }

    public Cursor getCategoryListWithNormalMenuPath() {
        Cursor cursor =null;

        cursor  = database.query(TableDefs.TABLE_CATEGORY,
                allColumns,  TableDefs.COLUMN_MENU_PATH_NORMAL +"=1", null, null, null, null);
        //new String[]{TableDefs.COLUMN_PARENT_AUTHOR,TableDefs.COLUMN_MESSAGE_ID,TableDefs.COLUMN_PARENT_MESSAGE,TableDefs.COLUMN_READ_STATUS}, TableDefs.COLUMN_USER_EMAIL +"=\""+GeneralVariables.userEmail+"\"",null, null,null, null);
        if(cursor != null)
            cursor.moveToFirst();
        Log.i("CategoryListDataSource", "No of categories " + cursor.getCount());


        return cursor;
    }

    public int getImageResource(String category) {
        int resourceId = 0;
        try {
            Cursor cursor = database.query(TableDefs.TABLE_CATEGORY, new String[]{
                            TableDefs.COLUMN_CATEGORY_IMAGE_ID},
                    TableDefs.COLUMN_CATEGORY_NAME + "=\'" + category + "\'",
                    null, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                resourceId = cursor.getInt(cursor.getColumnIndex(TableDefs.COLUMN_CATEGORY_IMAGE_ID));
            }

        } catch(Exception e) {
            Log.e("CategoryDataSource", "Error in fetching image Resource", e);
        }
        return resourceId;
    }

}