package com.localramu.android.app;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localramu.android.app.R;

/**
 * Created by mohitgupta on 06/02/16.
 */
public class StartPageStory1Fragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_start_page_story_1, container, false);

        return rootView;
    }
}
