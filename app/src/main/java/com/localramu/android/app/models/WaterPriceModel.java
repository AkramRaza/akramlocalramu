package com.localramu.android.app.models;

import com.google.gson.Gson;

/**
 * Created by mohitgupta on 23/04/16.
 */
public class WaterPriceModel {
    private String brand;
    private int price;
    private int litre;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getLitre() {
        return litre;
    }

    public void setLitre(int litre) {
        this.litre = litre;
    }

    public static WaterPriceModel fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, WaterPriceModel.class);
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
