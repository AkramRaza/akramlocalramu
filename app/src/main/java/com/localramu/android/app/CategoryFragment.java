package com.localramu.android.app;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.localramu.android.app.adapter.CategoryGridAdapter;
import com.localramu.android.app.db.CategoryDataSource;
import com.localramu.android.app.defs.TableDefs;
import com.localramu.android.app.utils.Utilities;


import java.sql.SQLException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CategoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CategoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryFragment extends Fragment {

    private static final String SELECTED_PIN_CODE = "SelectedPinCode";

    private GridView categoryGrid;
    private CategoryGridAdapter adaptor;
    private Cursor dataCursor;
    private CategoryDataSource datasource;

    private String selectedPinCode;

    private OnFragmentInteractionListener mListener;

    public CategoryFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param selectedPinCode Selected PinCode
     * @return A new instance of fragment CategoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CategoryFragment newInstance(String selectedPinCode) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString(SELECTED_PIN_CODE, selectedPinCode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedPinCode = getArguments().getString(SELECTED_PIN_CODE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_category, container, false);
        categoryGrid = (GridView) rootView.findViewById(R.id.categoryGrid);
        datasource = new CategoryDataSource(getActivity().getBaseContext());
        try {
            datasource.write();
            datasource.init();
            dataCursor = datasource.getCategoryList();
            datasource.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }



        if(dataCursor != null) {
            adaptor = new CategoryGridAdapter(getActivity().getBaseContext(), dataCursor);
            categoryGrid.setAdapter(adaptor);
        }

        categoryGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Cursor cursor = (Cursor) categoryGrid.getItemAtPosition(position);
                String selectedCategory = cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_CATEGORY_NAME));
                int menuPathNormal = cursor.getInt(cursor.getColumnIndex(TableDefs.COLUMN_MENU_PATH_NORMAL));

                if(Utilities.getSelectedAddress(getActivity().getApplicationContext()) == null) {
                    ((DashboardActivity) getActivity()).setAddress();
                    return;
                }

                Intent intent = null;
                if(menuPathNormal == 1) {
                    //intent = new Intent(getActivity().getBaseContext(),ServicePartnerActivity.class);
                    intent = new Intent(getActivity().getBaseContext(),Description.class);
                    intent.putExtra("position",position);
                } else {
                    switch(selectedCategory) {
                        case "Laundry" :
                            intent = new Intent(getActivity().getBaseContext(),LaundryPriceChartActivity.class);
                            break;
                        default :
                            intent = new Intent(getActivity().getBaseContext(),ExpressBookingActivity.class);
                    }

                }

                intent.putExtra("SELECTED_CATEGORY", selectedCategory);
                startActivity(intent);
                //GeneralVariables.choosenCategory = cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_TRUE_CAT_NAME));


            }

        });
        return rootView;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
