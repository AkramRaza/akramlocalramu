package com.localramu.android.app.utils;

import com.localramu.android.app.models.LaundryPriceModel;
import com.localramu.android.app.models.OfferSettingModel;
import com.localramu.android.app.models.WaterPriceModel;
import com.localramu.common.messages.codes.CustomerAppConfigKey;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohitgupta on 23/04/16.
 */
public class AppConfig {

    private String config;
    public AppConfig(final String config) {
        this.config = config;
    }

    public Integer getVersion() {
        Integer version = null;
        try {
            JSONObject reader = new JSONObject(config);
            version = reader.getInt(CustomerAppConfigKey.VERSION);
        } catch (Exception e) {

        }
        return version;
    }

    public List<WaterPriceModel> getWaterCanPrice() {
        List<WaterPriceModel> waterPrices = new ArrayList<WaterPriceModel>();
        try {
            JSONObject reader = new JSONObject(config);
            JSONObject priceChartJson = reader.getJSONObject(CustomerAppConfigKey.PRICE_CHART);
            JSONArray waterPriceArray = priceChartJson.getJSONArray(CustomerAppConfigKey.WATER_PRICES);
            for (int i = 0; i < waterPriceArray.length(); i++) {
                JSONObject waterPriceJson = waterPriceArray.getJSONObject(i);
                waterPrices.add(WaterPriceModel.fromJson(waterPriceJson.toString()));
            }
        } catch (Exception e) {

        }
        return waterPrices;
    }

    public OfferSettingModel getWaterCanOfferSetting() {
        OfferSettingModel offerSettingModel = null;
        try {
            JSONObject reader = new JSONObject(config);
            JSONObject offerSettingJson = reader.getJSONObject(CustomerAppConfigKey.OFFER_SETTING);
            JSONObject waterCanOffer = offerSettingJson.getJSONObject(CustomerAppConfigKey.WATER_CANS);
            offerSettingModel = OfferSettingModel.fromJson(waterCanOffer.toString());
        } catch(Exception e) {

        }
        return offerSettingModel;
    }

    public List<LaundryPriceModel> getLaundaryPrice() {
        List<LaundryPriceModel> laundaryPrices = new ArrayList<LaundryPriceModel>();
        try {
            JSONObject reader = new JSONObject(config);
            JSONObject priceChartJson = reader.getJSONObject(CustomerAppConfigKey.PRICE_CHART);
            JSONArray laundryPriceArray = priceChartJson.getJSONArray(CustomerAppConfigKey.LAUNDRY_PRICES);
            for (int i = 0; i < laundryPriceArray.length(); i++) {
                JSONObject laundryPriceJson = laundryPriceArray.getJSONObject(i);
                laundaryPrices.add(LaundryPriceModel.fromJson(laundryPriceJson.toString()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return laundaryPrices;
    }
}
