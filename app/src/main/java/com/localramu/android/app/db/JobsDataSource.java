package com.localramu.android.app.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.localramu.android.app.defs.TableDefs;
import com.localramu.android.app.utils.Logger;
import com.localramu.common.messages.model.Job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mohitgupta on 21/02/16.
 */
public class JobsDataSource {

    private SQLiteDatabase database;
    private DBHelper dbHelper;
    public JobsDataSource(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void write() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void read() throws SQLException {
        database = dbHelper.getReadableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Job getJob(String jobId) {
        Job job = null;
        Cursor cursor = database.query(TableDefs.TABLE_JOB, new String[]{
                        TableDefs.COLUMN_JOB_DETAILS}, TableDefs.COLUMN_JOB_ID + "=\"" + jobId + "\"",
                null, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        else
            return null;
        if (cursor.getCount() > 0){
            String jobInfoJson = cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_JOB_DETAILS));
            job = Job.fromJson(jobInfoJson);
        }
        return job;
    }

    public boolean createJob(Job job) {

        ContentValues values = new ContentValues();
        values.put(TableDefs.COLUMN_JOB_ID, job.getJobId());
        values.put(TableDefs.COLUMN_JOB_DETAILS,job.toJson());
        values.put(TableDefs.COLUMN_JOB_STATUS, job.getJobStatus().toString());

        long id=database.insert(TableDefs.TABLE_JOB, null, values);
        if(id==1) {
            Logger.z("JobsDataSource", String.format("Successfully inserted in new job with jobId - %s, "
                    , job.getJobId()));
            return true;
        }
        else{
            Logger.z(4,"JobsDataSource","Job Creation Unsuccessfull "+id);
            return false;
        }
    }



    public void updateJob(Job job) {
        ContentValues values = new ContentValues();
        values.put(TableDefs.COLUMN_JOB_DETAILS, job.toJson());
        values.put(TableDefs.COLUMN_JOB_STATUS, job.getJobStatus().toString());
        database.update(TableDefs.TABLE_JOB, values, TableDefs.COLUMN_JOB_ID + "=\"" + job.getJobId() + "\"", null);
    }





    public List<Job> getAllJobs(String status) {

        Cursor cursor = database.query(TableDefs.TABLE_JOB, new String[] {
                        TableDefs.COLUMN_JOB_DETAILS}, TableDefs.COLUMN_JOB_STATUS +"=\'"+status+"\'",
                null, null, null, null, null);
        List<Job> jobList = new ArrayList<Job>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String jobJson = cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_JOB_DETAILS));
                try {
                    Job job = Job.fromJson(jobJson);
                    jobList.add(job);
                } catch(Exception e) {
                    Logger.z(2,"ServiceDataSource",String.format("Unable to parse Json for Job Detail - {}", jobJson));
                }
            }
        }
        return jobList;

    }

    public List<Job> getAllJobs() {
        Cursor cursor = database.query(TableDefs.TABLE_JOB, new String[] {
                        TableDefs.COLUMN_JOB_DETAILS},
                null, null, null, null, null);
        List<Job> jobList = new ArrayList<Job>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String jobJson = cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_JOB_DETAILS));
                try {
                    Job job = Job.fromJson(jobJson);
                    jobList.add(job);
                } catch(Exception e) {
                    Logger.z(2,"ServiceDataSource",String.format("Unable to parse Json for Job Detail - {}", jobJson));
                }
            }
        }
        return jobList;

    }

    public void updateJobList(List<Job> jobList) {
        for(Job job : jobList) {
            if(getJob(job.getJobId()) == null) {
                createJob(job);
            } else {
                updateJob(job);
            }
        }
    }
}
