package com.localramu.android.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewBoldFont extends TextView{
	public TextViewBoldFont(Context context) {
        super(context);
        setFont();

    }

    public TextViewBoldFont (Context context, AttributeSet attrs) {
        super( context, attrs );
        setFont();
    }

    public TextViewBoldFont (Context context, AttributeSet attrs, int defStyle) {
        super( context, attrs, defStyle );
        setFont();
    }

    private void setFont() {
        Typeface normal = Typeface.createFromAsset(getContext().getAssets(),"fonts/MuseoSansRounded/MuseoSansRounded-700.otf");
        setTypeface( normal, Typeface.NORMAL );

    }
}
