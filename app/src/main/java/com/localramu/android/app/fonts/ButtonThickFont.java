package com.localramu.android.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class ButtonThickFont extends Button{
	  public ButtonThickFont(Context context) {
	        super( context );
	        setFont();

	    }

	    public ButtonThickFont(Context context, AttributeSet attrs) {
	        super( context, attrs );
	        setFont();
	    }

	    public ButtonThickFont(Context context, AttributeSet attrs, int defStyle) {
	        super( context, attrs, defStyle );
	        setFont();
	    }

	    private void setFont() {
	        Typeface normal = Typeface.createFromAsset(getContext().getAssets(),"fonts/MuseoSansRounded/MuseoSansRounded-500.otf");
	        setTypeface( normal, Typeface.NORMAL );

	    }}
