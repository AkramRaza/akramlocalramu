package com.localramu.android.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.localramu.android.app.defs.SharedPreferencesKeys;
import com.localramu.android.app.gcm.GcmRegistrationIntentService;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.model.UserInfo;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class StartActivity extends FragmentActivity {


    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    private ImageView dot1, dot2, dot3, dot4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start);

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        //findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        dot1 = (ImageView)findViewById(R.id.dot_1);
        dot2 = (ImageView)findViewById(R.id.dot_2);
        dot3 = (ImageView)findViewById(R.id.dot_3);
        dot4 = (ImageView)findViewById(R.id.dot_4);

        dotChager(mPager);

        new InitProcess().execute();

    }

    private void dotChager(ViewPager viewPager) {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {

                switch (position) {
                    case 0:
                        dot1.setImageResource(R.drawable.dot_selected);
                        dot2.setImageResource(R.drawable.dot);
                        break;

                    case 1:
                        dot1.setImageResource(R.drawable.dot);
                        dot2.setImageResource(R.drawable.dot_selected);
                        dot3.setImageResource(R.drawable.dot);
                        break;

                    case 2:
                        dot2.setImageResource(R.drawable.dot);
                        dot3.setImageResource(R.drawable.dot_selected);
                        dot4.setImageResource(R.drawable.dot);
                        break;

                    case 3:
                        dot3.setImageResource(R.drawable.dot);
                        dot4.setImageResource(R.drawable.dot_selected);
                        break;

                    default:
                        break;
                }


            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch(position) {
                case 0: fragment = new StartPageFragment();
                    break;
                case 1: fragment = new StartPageStory1Fragment();
                    break;
                case 2: fragment = new StartPageStory2Fragment();
                    break;
                case 3 : fragment = new StartPageStory3Fragment();

            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    public void logIn(View view) {
        Intent intent = new Intent(this, UserLoginActivity.class);
        startActivity(intent);
    }

    public void signUp(View view) {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }

    private void initialise() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedPreferences.edit().putBoolean(SharedPreferencesKeys.INTERNET_CONNECTED, true).apply();
        UserInfo userInfo = Utilities.getUserInfo(getApplicationContext());
        if(userInfo != null)  {
            if(!userInfo.isGcmUpdatedOnServer()) {
                Intent msgIntent = new Intent(StartActivity.this, GcmRegistrationIntentService.class);
                startService(msgIntent);
            }
            Intent intent = new Intent(getBaseContext(),DashboardActivity.class);
            startActivity(intent);
            finish();
        }

    }

    private class InitProcess extends AsyncTask<Void, Void, UserInfo> {

        @Override
        protected UserInfo doInBackground(Void... params) {

            UserInfo userInfo = Utilities.getUserInfo(getApplicationContext());

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            sharedPreferences.edit().putBoolean(SharedPreferencesKeys.INTERNET_CONNECTED, true).apply();
            //Thread.sleep(1200);
            return userInfo;
        }

        @Override
        protected void onPostExecute(UserInfo userInfo) {

            if(userInfo != null) {
                if(!userInfo.isGcmUpdatedOnServer()) {
                    Intent msgIntent = new Intent(StartActivity.this, GcmRegistrationIntentService.class);
                    startService(msgIntent);
                }
                Intent intent = new Intent(getBaseContext(),LoadingConfigActivity.class);
                startActivity(intent);
                StartActivity.this.finish();

            }

        }

    }

}
