package com.localramu.android.app;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.localramu.android.app.adapter.BookingListAdapter;
import com.localramu.android.app.background.FeedbackProcess;
import com.localramu.android.app.background.GetJobListProcess;
import com.localramu.android.app.background.LogoutProcess;
import com.localramu.android.app.defs.GeneralDefs;
import com.localramu.android.app.models.Address;
import com.localramu.android.app.utils.Constants;
import com.localramu.android.app.utils.JobSortingUtil;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.JobStatus;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.model.Job;
import com.localramu.common.messages.model.UserInfo;
import com.localramu.common.messages.service.request.FeedbackRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,CategoryFragment.OnFragmentInteractionListener{

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    static final int ADDRESS_FIND_REQUEST = 1;  // The request code
    private SharedPreferences sharedpreferences;
    private ObjectAnimator animation;
    private List<String> jobProcessList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Express Booking Service of LocalRamu", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent intent = new Intent(DashboardActivity.this, ExpressBookingActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        sharedpreferences = getSharedPreferences(Constants.LOCAL_RAMU, Context.MODE_PRIVATE);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        CategoryFragment categoryFragment = CategoryFragment.newInstance("9090");
        getFragmentManager().beginTransaction()
                .add(R.id.container, new CategoryFragment()).commit();
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        Address address = Utilities.getSelectedAddress(getApplicationContext());
        if(address == null) {
            Intent intent = new Intent(this, AddressSearchActivity.class);
            startActivityForResult(intent, ADDRESS_FIND_REQUEST);
        } else {
            getSupportActionBar().setTitle(address.getAddressSecondary());
        }

        UserInfo userInfo = Utilities.getUserInfo(getApplicationContext());

        if(userInfo != null) {
           View headerView =  ((NavigationView) findViewById(R.id.nav_view)).getHeaderView(0);
            ((TextView)headerView.findViewById(R.id.header_email)).setText(userInfo.getUserId());
            ((TextView)headerView.findViewById(R.id.header_full_name)).setText(userInfo.getFullName());

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Done by Akram on 24/8/2016
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_user) {
            /*Intent intent = new Intent(this, OffersActivity.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(),"We are having a lot of Offers for You!!",Toast.LENGTH_LONG).show();*/
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.feedback_vendor);
            Button ok= (Button) dialog.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
            return true;
        } else if(id == R.id.action_search_location) {
            // Handle the camera action
            Intent intent = new Intent(this, AddressSearchActivity.class);
            startActivityForResult(intent, ADDRESS_FIND_REQUEST);

            //startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_booking) {
            Intent intent = new Intent(this, BookingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_express_booking) {
            Intent intent = new Intent(this, ExpressBookingActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_offer) {
            // Handle the camera action
            Intent intent = new Intent(this, OffersActivity.class);
            startActivity(intent);
        }  else if (id == R.id.nav_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Download Local Ramu app on google play:- https://play.google.com/store/apps/details?id=com.localramu.android.app&hl=en";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Try a whole new experience of Local Ramu");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } else if (id == R.id.nav_feedback) {
            Intent intent = new Intent(this, FeedbackActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_contact_us) {
            Intent intent = new Intent(this, ContactUs.class);
            startActivity(intent);
        }else if (id == R.id.nav_logout) {
            handleLogout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetServiceListTask().execute();
    }



    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Dashboard Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.localramu.android.app/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Dashboard Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.localramu.android.app/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == ADDRESS_FIND_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String result=data.getStringExtra("result");
                if(result != null) {
                    getSupportActionBar().setTitle(result);
                }
            }
        }
    }

    private void showProgress() {
        // Locate view
        TextView ring = (TextView) findViewById(R.id.ring);

        animation = ObjectAnimator.ofFloat(ring, "rotationY", 0.0f, 360f);
        animation.setDuration(2000);
        animation.setRepeatCount(ObjectAnimator.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        animation.start();
    }

    private void cancelProgress() {
        if(animation != null) {
            animation.end();
        }
       // ((RelativeLayout) findViewById(R.id.custom_progress)).setVisibility(View.GONE);
    }

    public class GetServiceListTask extends  AsyncTask<Void, Void, List<Job>>

    {


        @Override
        protected void onPreExecute() { showProgress(); }
        @Override
        protected List<Job> doInBackground(Void... params) {
        UserInfo userInfo = Utilities.getUserInfo(getApplicationContext());
        if(userInfo != null) {
            GetJobListProcess getJobListProcess = new GetJobListProcess(userInfo.getUserId(),
                    getApplicationContext());
            return getJobListProcess.getJobs();
        } else {

            return null;
        }
    }


        @Override
        protected void onPostExecute(final List<Job> jobList) {
        String storedList = sharedpreferences.getString(Constants.BOOKING_HISTORY, null);
        if(storedList != null && !storedList.equals("null")) {
            jobProcessList = new Gson().fromJson(storedList, List.class);
        }
        for(final Job job : jobList)
        {
            if(job.getJobStatus().toString().equalsIgnoreCase(JobStatus.COMPLETED.toString()))
            {

                if(jobProcessList != null) {
                    Iterator<String> iter = jobProcessList.iterator();
                    while(iter.hasNext()){
                        String jobStoredId = iter.next();
                        if (jobStoredId.equals(job.getJobId())) {
                            final Dialog dialog = new Dialog(DashboardActivity.this);

                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.feedback_vendor);
                            dialog.setCanceledOnTouchOutside(false);
                            TextView orderName = (TextView) dialog.findViewById(R.id.order_name);
                            final EditText feedbackMessege = (EditText) dialog.findViewById(R.id.feedbackMessege);
                            final RatingBar feedbackServiceRatingBar = (RatingBar) dialog.findViewById(R.id.feedbackServiceRatingBar);
                            orderName.setText("Order Name : " + job.getJobDetails());
                            Button ok = (Button) dialog.findViewById(R.id.ok);
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sendFeedback(job, feedbackMessege.getText().toString(), "" + feedbackServiceRatingBar.getRating());
                                    dialog.dismiss();
                                }
                            });
                            jobProcessList.remove(jobStoredId);
                            dialog.show();
                        }
                    }
                }


            }
            if(job.getJobStatus().toString().equalsIgnoreCase(JobStatus.FLIGHT.toString()) && !jobProcessList.contains(job.getJobId()))
            {
                jobProcessList.add(job.getJobId());
            }

        }
        String jobListStr = new Gson().toJson(jobProcessList);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.BOOKING_HISTORY, jobListStr);
        editor.commit();

        cancelProgress();
    }

        @Override
        protected void onCancelled() {
        cancelProgress();
    }
    }

    public void setAddress() {
        Intent intent = new Intent(this, AddressSearchActivity.class);
        startActivityForResult(intent, ADDRESS_FIND_REQUEST);
    }

    private void handleLogout() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_lock_power_off)
                .setTitle("Alert")
                .setMessage("Do you Really want to logout.")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new LogoutProcess(getApplicationContext(), DashboardActivity.this).execute();
                    }
                })
                .setNegativeButton("No", null)
                .show();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }



    public void sendFeedback(final Job job, final String feedback, final String rating)
    {
        String deviceUserId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        FeedbackRequest feedbackRequest  =new FeedbackRequest();
        feedbackRequest.setJobId(job.getJobId());
        feedbackRequest.setRequestId(job.getCustomerId());
        feedbackRequest.setRequestingDevice(deviceUserId);
        feedbackRequest.setFeedback(feedback);
        feedbackRequest.setRating(rating);

        new AsyncTask<String, String, StatusCode>() {

            @Override
            protected StatusCode doInBackground(String... params) {


                FeedbackProcess feedbackProcess = new FeedbackProcess(feedback, rating, job,
                        getApplicationContext());
                return feedbackProcess.sendFeedback();

            }

            @Override
            protected void onPostExecute(final StatusCode statusCode) {
                super.onPostExecute(statusCode);
                try {
                    if (statusCode == StatusCode.SUCCESS) {
                        Toast.makeText(getApplicationContext(),
                                "Thanks for giving your valuable Feedback", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DashboardActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Sorry, Something went wrong!! Please Try again", Toast.LENGTH_LONG).show();
                        /*Intent intent =new Intent(FeedbackActivity.this,DashboardActivity.class);
                        startActivity(intent);*/
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }.execute();
    }



}
