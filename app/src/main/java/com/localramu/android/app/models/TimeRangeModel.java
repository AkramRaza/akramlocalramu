package com.localramu.android.app.models;

/**
 * Created by mohitgupta on 05/02/16.
 */
public class TimeRangeModel {
    private String timeString;
    private String fromTime;
    private String toTime;
    private int fromTimeInt;
    private int toTimeInt;

    public TimeRangeModel(String timeString, String fromTime, String toTime, int fromTimeInt, int toTimeInt) {
        this.timeString = timeString;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.fromTimeInt = fromTimeInt;
        this.toTimeInt = toTimeInt;
    }

    public String getTimeString() {
        return timeString;
    }

    public void setTimeString(String timeString) {
        this.timeString = timeString;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public int getFromTimeInt() {
        return fromTimeInt;
    }

    public void setFromTimeInt(int fromTimeInt) {
        this.fromTimeInt = fromTimeInt;
    }

    public int getToTimeInt() {
        return toTimeInt;
    }

    public void setToTimeInt(int toTimeInt) {
        this.toTimeInt = toTimeInt;
    }
}
