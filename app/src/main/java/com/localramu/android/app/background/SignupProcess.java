package com.localramu.android.app.background;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Log;


import com.google.gson.Gson;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.android.app.utils.MD5HashGenerator;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.LoginMethod;
import com.localramu.common.messages.enums.UserType;
import com.localramu.common.messages.model.UserInfo;
import com.localramu.common.messages.service.request.UserRegistrationRequest;
import com.localramu.common.messages.service.response.UserRegistrationResponse;
import com.localramu.common.url.UrlDefs;


public class SignupProcess {
	private String email,password,fullname, mobile, imageUrl;
	private LoginMethod loginMethod;
	private Context context;
	private UserInfoDataSource userInfoDataSource;
	
	public SignupProcess(String fullName, String email, String mobile, String password,Context context,
						 LoginMethod loginMethod, String imageUrl) {
		this.fullname = fullName;
		this.email = email;
		this.mobile = mobile;
		this.password = password;
		this.loginMethod = loginMethod;
		this.imageUrl = imageUrl;

		this.context = context;
		this.userInfoDataSource = new UserInfoDataSource(context);
	}


	public StatusCode signup() {

		StatusCode statusCode =  StatusCode.UNKNOWN;

		try{
			MD5HashGenerator md5HashGenerator = new MD5HashGenerator();
			String passwordHash = md5HashGenerator.generatHash(password);
			String deviceUserId = Secure.getString(context.getContentResolver(),
					Secure.ANDROID_ID); 
			
			UserInfo userInfo = new UserInfo();
			userInfo.setUserId(email);
			userInfo.setPassword(passwordHash);
			userInfo.setFullName(fullname);
			userInfo.setEmail(email);
			userInfo.setMobileNumber(mobile);
			userInfo.setDeviceId(deviceUserId);
			userInfo.setUserType(UserType.CUSTOMER);
			userInfo.setLoginMethod(loginMethod);
			if(imageUrl != null) {
				userInfo.setImageUrl(imageUrl);
			}

			RestTemplate restTemplate = new RestTemplate();
			UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
			userRegistrationRequest.setUserInfo(userInfo);
			restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
			
			Gson gson = new Gson();
			Log.i("Request",gson.toJson(userRegistrationRequest));
			
			UserRegistrationResponse response = restTemplate.postForObject(UrlDefs.USER_REGISTRATION_URL, userRegistrationRequest, UserRegistrationResponse.class);
	     			Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());

	     			statusCode = response.getStatusCode();
	        if(response.isSuccess() && response.getStatusCode() == StatusCode.SUCCESS) {
	        	userInfoDataSource.write();
	        	userInfoDataSource.logoutAllUser();
				userInfoDataSource.createUser(userInfo, loginMethod.name(),1);
				userInfoDataSource.close();
	        } 
		}
		catch(Exception e){
			e.printStackTrace();
			statusCode = StatusCode.ERROR_OCCURRED;
		}
		return statusCode;
	}

}
