package com.localramu.android.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by LENOVO on 8/13/2016.
 */
public class SelectAddress extends AppCompatActivity {
    EditText addr1;
    EditText addr2;
    EditText addr3;
    EditText addr4;
    Button saveAddress;
    String fullAddress;
    String flatNo;
    String streetAddress;
    String locality;
    String city;
    public static final String LOCALRAMUPREF = "LocalRamuPref" ;
    SharedPreferences sharedpreferences;
    Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_address);
        addr1= (EditText)findViewById(R.id.addr1);
        addr2= (EditText)findViewById(R.id.addr2);
        addr3= (EditText)findViewById(R.id.addr3);
        addr4= (EditText)findViewById(R.id.addr4);
        saveAddress= (Button)findViewById(R.id.saveAddress);
        sharedpreferences = getSharedPreferences(LOCALRAMUPREF, Context.MODE_PRIVATE);

        saveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeAddress(view);
                Intent intent=new Intent(SelectAddress.this,DashboardActivity.class);
                startActivity(intent);
            }
        });



    }

    public void takeAddress(View view){

        flatNo = addr1.getText().toString().trim();
        streetAddress = addr2.getText().toString().trim();
        locality = addr3.getText().toString().trim();
        city = addr4.getText().toString().trim();
        fullAddress= flatNo+", "+streetAddress+", "+locality+city ;
        if(flatNo.matches("")||streetAddress.matches("")||locality.matches("")){
            Snackbar.make(view, "Please Fill Your Service Delivery Address", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }else {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("addressKey",fullAddress);
            editor.commit();
            String abc=sharedpreferences.getString("addressKey","");
            System.out.println("******"+fullAddress);
            System.out.println("**1****"+abc);
        }


    }
}
