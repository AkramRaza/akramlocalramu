package com.localramu.android.app;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.localramu.android.app.adapter.OffersListAdapter;
import com.localramu.android.app.adapter.ServicePartnerListAdapter;
import com.localramu.android.app.background.GetOffersProcess;
import com.localramu.android.app.background.GetServiceListProcess;
import com.localramu.android.app.ui.RecyclerItemClickListener;
import com.localramu.common.messages.model.Offer;
import com.localramu.common.messages.model.Service;

import java.util.List;

public class OffersActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String category;
    private ProgressDialog progressDialog;
    private ObjectAnimator animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        showProgress();

        mapResource();

        new GetOffersTask().execute();

    }


    private void mapResource () {

        mRecyclerView = (RecyclerView) findViewById(R.id.offer_list);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        view.getTag();
                    }
                })
        );
    }

    /**
     * Represents an asynchronous to get the list of Services
     */
    public class GetOffersTask extends AsyncTask<Void, Void, List<Offer>> {



        @Override
        protected void onPreExecute() {
        }
        @Override
        protected List<Offer> doInBackground(Void... params) {
            GetOffersProcess getOffersProcess = new GetOffersProcess(getApplicationContext());
            return getOffersProcess.getOffers();
        }

        @Override
        protected void onPostExecute(final List<Offer> offers) {
            if(offers != null && offers.size() != 0) {
                // specify an adapter (see also next example)
                mAdapter = new OffersListAdapter(offers,getApplicationContext(), OffersActivity.this);
                mRecyclerView.setAdapter(mAdapter);
            } else {
                OffersActivity.this.findViewById(R.id.error).setVisibility(View.VISIBLE);
            }
            cancelProgress();
        }

        @Override
        protected void onCancelled() {
            cancelProgress();
        }
    }

    private void showProgress() {
        // Locate view
        TextView ring = (TextView) findViewById(R.id.ring);

        animation = ObjectAnimator.ofFloat(ring, "rotationY", 0.0f, 360f);
        animation.setDuration(2000);
        animation.setRepeatCount(ObjectAnimator.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        animation.start();
    }

    private void cancelProgress() {
        if(animation != null) {
            animation.end();
        }
        ((RelativeLayout) findViewById(R.id.custom_progress)).setVisibility(View.GONE);
    }


}
