package com.localramu.android.app.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.localramu.android.app.defs.TableDefs;


/**
 * Created by mohitgupta on 02/02/16.
 */
public class DBHelper extends SQLiteOpenHelper {


    private static final String DATABASE_CREATE_USER_INFO = "create table "
            + TableDefs.TABLE_USER_INFO + "(" + TableDefs.COLUMN_USER_EMAIL
            + " text primary key, " + TableDefs.COLUMN_USER_PASSWORD
            + " text, " + TableDefs.COLUMN_FULL_NAME
            + " text, " + TableDefs.COLUMN_IMAGE_URL
            + " text, " + TableDefs.COLUMN_MOBILE_NUMBER
            + " text, " + TableDefs.COLUMN_LOGIN_METHOD
            + " text , " + TableDefs.COLUMN_IS_LOGGED_IN
            + " integer not null, " + TableDefs.COLUMN_IS_GCM_UPDATED
            + " integer not null, " + TableDefs.COLUMN_GCM_ID
            + " text, " + TableDefs.COLUMN_DEVICE_ID
            + " text, " + TableDefs.COLUMN_LAST_LOG_IN_TIME
            + " text not null) ;";


    private static final String DATABASE_CREATE_CATEGORY_LIST = "create table "
            + TableDefs.TABLE_CATEGORY + "(" + TableDefs.COLUMN_ID
            + " int primary key, " + TableDefs.COLUMN_CATEGORY_NAME
            + " text not null, " + TableDefs.COLUMN_CATEGORY_DETAILS
            + " text, " + TableDefs.COLUMN_NEXT_ACTIVITY
            + " text, " + TableDefs.COLUMN_CATEGORY_IMAGE_ID
            + " int not null, " + TableDefs.COLUMN_MENU_PATH_NORMAL
            + " int not null, " + TableDefs.COLUMN_IS_CATEGORY_ENABLED
            + " int not null, " + TableDefs.COLUMN_IS_DESCRIPTION
            + " int not null); ";

    private static final String DATABASE_CREATE_ADDRESS = "create table "
            + TableDefs.TABLE_ADDRESS + "(" + TableDefs.COLUMN_ID
            + " text primary key, " + TableDefs.COLUMN_ADDRESS_TYPE
            + " text not null , " + TableDefs.COLUMN_ADDRESS_PRIMARY
            + " text unique, " + TableDefs.COLUMN_ADDRESS_SECONDARY
            + " text, " + TableDefs.COLUMN_LOCALITY
            + " text, " + TableDefs.COLUMN_CITY
            + " text, " + TableDefs.COLUMN_LATITUDE
            + " decimal, " + TableDefs.COLUMN_LONGITUDE
            + " decimal, " + TableDefs.COLUMN_LANDMARK
            + " decimal, " + TableDefs.COLUMN_MOBILE_NUMBER
            + " text); ";

    private static final String DATABASE_CREATE_JOB = "create table "
            + TableDefs.TABLE_JOB + "(" + TableDefs.COLUMN_JOB_ID
            + " int primary key, " + TableDefs.COLUMN_JOB_DETAILS
            + " text not null , " + TableDefs.COLUMN_JOB_STATUS
            + " text not null , " + TableDefs.COLUMN_CREATION_TIME
            + " text); ";

    public DBHelper(Context context) {
        super(context, TableDefs.DATABASE_NAME, null, TableDefs.DATA_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        // TODO Auto-generated method stub
        database.execSQL(DATABASE_CREATE_USER_INFO);
        database.execSQL(DATABASE_CREATE_CATEGORY_LIST);
        database.execSQL(DATABASE_CREATE_ADDRESS);
        database.execSQL(DATABASE_CREATE_JOB);
        Log.i("DBHelper", "DataBase Created Successfully");

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int arg1, int arg2) {
        // TODO Auto-generated method stub

        database.execSQL("DROP TABLE IF EXISTS '" + TableDefs.TABLE_USER_INFO + "'");
        database.execSQL("DROP TABLE IF EXISTS '" + TableDefs.TABLE_ADDRESS + "'");
        database.execSQL("DROP TABLE IF EXISTS '" + TableDefs.TABLE_CATEGORY + "'");
        database.execSQL("DROP TABLE IF EXISTS '" + TableDefs.TABLE_JOB + "'");
        onCreate(database);
        Log.i("DBHelper", "Upgrade Successfull");
    }

}