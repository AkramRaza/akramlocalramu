package com.localramu.android.app.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.localramu.android.app.R;
import com.localramu.android.app.ServiceDetailActivity;
import com.localramu.android.app.models.Address;
import com.localramu.android.app.utils.DistanceCalculator;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.model.Offer;
import com.localramu.common.messages.model.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by mohitgupta on 06/02/16.
 */

public class OffersListAdapter extends RecyclerView.Adapter<OffersListAdapter.ViewHolder> {
    private List<Offer> mDataset;
    private Context mContext;
    private Activity mActivity;
    private DistanceCalculator distanceCalculator = new DistanceCalculator();




    // Provide a suitable constructor (depends on the kind of dataset)
    public OffersListAdapter(List<Offer> mDataset, Context context, Activity mActivity) {
        this.mDataset = mDataset;
        this.mContext =  context;
        this.mActivity = mActivity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OffersListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.component_offer_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        Offer offer = mDataset.get(position);
        String category = offer.getCategory();
        holder.category.setText(category);
        holder.title.setText(offer.getTitle());
        holder.description.setText(offer.getDescription());

        Date validity = new Date(offer.getValidity() );
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        holder.validity.setText("Valid Till : "+date.format(validity));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder{
        protected TextView category;
        protected TextView title;
        protected TextView description;
        protected TextView validity;


        public ViewHolder(View view) {
            super(view);
            this.category = (TextView) view.findViewById(R.id.category);
            this.title = (TextView) view.findViewById(R.id.offer_title);
            this.description = (TextView) view.findViewById(R.id.offer_description);
            this.validity = (TextView) view.findViewById(R.id.validity);
        }


     }



}