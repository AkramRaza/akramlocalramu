package com.localramu.android.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.localramu.android.app.background.GetJobProcess;
import com.localramu.android.app.db.JobsDataSource;
import com.localramu.android.app.utils.JobStatusMapper;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.ExternalJobStatus;
import com.localramu.common.messages.model.Job;

public class BookingDetailsActivity extends AppCompatActivity {

    private String jobId;
    TextView cancel;
    TextView uCancel;
    TextView jobStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);
        Intent intent = getIntent();
        jobId = intent.getStringExtra("JOB_ID");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        cancel=(TextView) findViewById(R.id.cancel);
        uCancel=(TextView) findViewById(R.id.uCancel);
        jobStatus=(TextView) findViewById(R.id.job_status);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(BookingDetailsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.cancel_order);
                dialog.show();
                final CheckBox ch1=(CheckBox)dialog.findViewById(R.id.checkBox1);
                final CheckBox ch2=(CheckBox)dialog.findViewById(R.id.checkBox2);
                final CheckBox ch3=(CheckBox)dialog.findViewById(R.id.checkBox3);

                Button cancelReason= (Button) dialog.findViewById(R.id.submitCancelReason);

                cancelReason.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(ch1.isChecked()||ch2.isChecked()||ch3.isChecked()){
                            Toast.makeText(getApplicationContext(),"Your Order Canceled!! Happy to serve you in your next order",
                                    Toast.LENGTH_LONG).show();
                            cancel.setVisibility(View.GONE);
                            uCancel.setVisibility(View.VISIBLE);
                            jobStatus.setVisibility(View.GONE);
                            /*Intent i = new Intent(BookingDetailsActivity.this,DashboardActivity.class);
                            startActivity(i);*/
                            dialog.dismiss();
                        }else{
                            Snackbar.make(v,"Please Give Atleast One Reason",
                                    Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }

                    }
                });
            }
        });
        setSupportActionBar(toolbar);

    }

    @Override
    protected void onResume() {
        super.onResume();
        new LoadJobDetails(jobId).execute();
    }

    private void onCancel(){
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }.execute();


    }

    private void populateFields(final Job job) {
        if(job.getAttachedVendorName() != null && !job.getAttachedVendorName().equals("")) {
            ((TextView) findViewById(R.id.full_name)).setText(job.getAttachedVendorName());
        } else {
            ((TextView) findViewById(R.id.full_name)).setText("Awaiting Vendor Confirmation");
        }
        if(job.getAttachedVendorDisplayName() != null && !job.getAttachedVendorDisplayName().equals("")) {
            ((TextView) findViewById(R.id.display_name)).setText(job.getAttachedVendorDisplayName());
        } else {
            ((TextView) findViewById(R.id.display_name)).setText(job.getCategory());
        }
        ((TextView) findViewById(R.id.job_details)).setText(job.getJobDetails());
        ((TextView) findViewById(R.id.schedule_date)).setText(job.getJobRequestDate());
        ((TextView) findViewById(R.id.schedule_time)).setText(job.getJobRequestTime());
        ((TextView) findViewById(R.id.address)).setText(job.getCustomerAddress());

        ((TextView) findViewById(R.id.payement_status)).setText(job.getPaymentStatus().name());
        ((TextView) findViewById(R.id.price_quote)).setText(String.valueOf(job.getPriceQuote()));

        int resourceId = Utilities.getImageResource(job.getCategory(), getApplicationContext());

        if(resourceId == 0) {
            resourceId = R.drawable.logo;
        }
        ((ImageView) findViewById(R.id.service_partner_image)).setBackgroundResource(resourceId);

        TextView jobStatusView = (TextView) findViewById(R.id.job_status);

        ExternalJobStatus jobStatus = JobStatusMapper.findExternalJobStatus(job.getJobStatus());
        jobStatusView.setText(jobStatus.name());

        if(jobStatus == ExternalJobStatus.COMPLETED) {
            jobStatusView.setBackgroundResource(R.drawable.completed_label);
            cancel.setVisibility(View.GONE);
        } else if(jobStatus == ExternalJobStatus.PROCESSING || jobStatus == ExternalJobStatus.ACCEPTED) {
            jobStatusView.setBackgroundResource(R.drawable.processing_label);
        } else {
            jobStatusView.setBackgroundResource(R.drawable.cancelled_label);
            cancel.setVisibility(View.GONE);
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = null;
                if(job.getAttachedVendorId() != null) {
                    url  = "tel:"+job.getAttachedVendorId();
                } else {
                    url  = "tel:8792341573";
                }
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }
    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class LoadJobDetails extends AsyncTask<Void, Void, Job> {

        private String mJobId;
        LoadJobDetails(String jobId) {
            this.mJobId = jobId;
        }

        @Override
        protected Job doInBackground(Void... params) {
            JobsDataSource jobsDataSource = new JobsDataSource(getApplicationContext());
            jobsDataSource.read();
            Job job  = jobsDataSource.getJob(mJobId);
            jobsDataSource.close();
            return job;
        }

        @Override
        protected void onPostExecute(final Job job) {
            if(job != null) {
                populateFields(job);
            }
        }

        @Override
        protected void onCancelled() {

        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class RefreshJobDetails extends AsyncTask<Void, Void, Job> {

        private String mJobId;
        RefreshJobDetails(String jobId) {
            this.mJobId = jobId;
        }

        @Override
        protected Job doInBackground(Void... params) {
            GetJobProcess getJobProcess = new GetJobProcess(mJobId, getApplicationContext());
            return getJobProcess.getJob();
        }

        @Override
        protected void onPostExecute(final Job job) {
            if(job != null) {
                populateFields(job);
            }
        }

        @Override
        protected void onCancelled() {

        }
    }
}
