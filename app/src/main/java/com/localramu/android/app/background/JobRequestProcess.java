package com.localramu.android.app.background;

import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.JobsDataSource;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.android.app.utils.MD5HashGenerator;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.UserType;
import com.localramu.common.messages.model.Job;
import com.localramu.common.messages.service.request.CreateJobRequest;
import com.localramu.common.messages.service.request.LoginRequest;
import com.localramu.common.messages.service.response.CreateJobResponse;
import com.localramu.common.messages.service.response.LoginResponse;
import com.localramu.common.url.UrlDefs;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;


public class JobRequestProcess {

	private Job job;
	private Context context;
	private JobsDataSource jobsDataSource;

	public JobRequestProcess(Job job, Context context) {
		this.job = job;
		this.context = context;
		this.jobsDataSource = new JobsDataSource(context);
	}

	public StatusCode createJob() {
		StatusCode statusCode = StatusCode.UNKNOWN;;
		try{

			String deviceUserId = Secure.getString(context.getContentResolver(),
					Secure.ANDROID_ID); 
			RestTemplate restTemplate = new RestTemplate();
			CreateJobRequest createJobRequest = new CreateJobRequest();
			createJobRequest.setJob(job);
			createJobRequest.setRequestingDevice(deviceUserId);

			restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
			JSONObject json = new JSONObject();
			Gson gson = new Gson();
			
			Log.i("Request",  gson.toJson(createJobRequest));
			CreateJobResponse response = restTemplate.postForObject(UrlDefs.CREATE_JOB_URL,
					createJobRequest, CreateJobResponse.class);
			Log.i("Response",  gson.toJson(response));

			statusCode = response.getStatusCode();
			if(response.isSuccess()) {
				jobsDataSource.write();
				jobsDataSource.createJob(job);
				jobsDataSource.close();
			}
			Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());

		}
		catch(Exception e){
			e.printStackTrace();
			statusCode = StatusCode.ERROR_OCCURRED;
		}
		return statusCode;

	}
}
