package com.localramu.android.app.ui;




import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.localramu.android.app.R;

public class CustomToast {

	
	public static void makeToast(int type,Context context,String msg,Activity activity) {
		   if (type==1) {
		          Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
		   } else {
		          LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);;
		          View layout;
		          switch(type){
		          case 3 :layout = inflater.inflate(R.layout.custom_toast_red, (ViewGroup)(activity.findViewById(R.id.toast_layout_root)));
	                 break;
		          case 4 :layout = inflater.inflate(R.layout.custom_toast_green, (ViewGroup)(activity.findViewById(R.id.toast_layout)));
	                 break;
		          default:layout = inflater.inflate(R.layout.custom_toast, (ViewGroup)(activity.findViewById(R.id.toast_layout)));
	                 break;
	         }
		          TextView toastmsg = (TextView) layout.findViewById(R.id.toast_text);
 		          toastmsg.setText(msg);
		          Toast toast = new Toast(context);
		          toast.setDuration(Toast.LENGTH_SHORT);
		          toast.setView(layout);
		          toast.show();
		    }
		  }
}
