package com.localramu.android.app.background;

import android.content.Context;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.localramu.android.app.db.AddressDataSource;
import com.localramu.android.app.defs.GeneralDefs;
import com.localramu.android.app.models.Address;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by mohitgupta on 17/02/16.
 */


public class GeoPointTask extends AsyncTask<Void, Void, LatLng> {

    private final Address mAddress;
    private final Context mContext;

    public GeoPointTask(Address address, Context context) {
        mAddress = address;
        mContext = context;
    }

    @Override
    protected LatLng doInBackground(Void... params) {
        Geocoder coder = new Geocoder(mContext);
        List<android.location.Address> address;
        LatLng p1 = null;
        JSONObject jsonObject = null;

        try {

            String fullAddress = mAddress.getAddressPrimary() +","+ mAddress.getAddressSecondary();
            fullAddress = fullAddress.replaceAll(" ","%20");
            String requestURL = "http://maps.google.com/maps/api/geocode/json?address=" + fullAddress + "&sensor=false";

            URL url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            int responseCode=conn.getResponseCode();
            String response = "";
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
                jsonObject = new JSONObject(response);
                p1 = getLatLong(jsonObject);
            }
            else {
                response="";

            }
            Log.i("TAG", response);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return p1;
    }

    @Override
    protected void onPostExecute(final LatLng latLng) {
        if(latLng != null) {
            AddressDataSource addressDataSource = new AddressDataSource(mContext);
            addressDataSource.write();
            if(mAddress.getAddressSecondary() != null) {
                String splits[] = mAddress.getAddressSecondary().split(",");
                if(splits.length > 0) {
                    mAddress.setLocality(splits[0]);
                }
            }
            mAddress.setLatitude(latLng.latitude);
            mAddress.setLongitude(latLng.longitude);
            addressDataSource.createAddress(mAddress);
            GeneralDefs.currentSelectedAddress = mAddress;
            addressDataSource.close();
        }
    }




    private  LatLng getLatLong(JSONObject jsonObject) {

        LatLng latLng = null;
        try {
            Log.i("TAG", jsonObject.toString());
            if(jsonObject != null) {
                double longitute = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");
                double latitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");
                latLng = new LatLng(latitude, longitute);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return latLng;
    }
}