package com.localramu.android.app.utils;

import com.localramu.android.app.db.AddressDataSource;
import com.localramu.android.app.db.CategoryDataSource;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.android.app.defs.GeneralDefs;
import com.localramu.android.app.models.Address;
import com.localramu.common.messages.model.UserInfo;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;


public class Utilities {


	public static UserInfo getUserInfo(Context context) {
		UserInfo userInfo = null;
		UserInfoDataSource datasource = new UserInfoDataSource(context);
		datasource.read();
		userInfo = datasource.getUserInfo();
		datasource.close();
		return userInfo;

	}

	public static Address getSelectedAddress(Context context) {
		Address address = GeneralDefs.currentSelectedAddress;
		if(address == null) {
			AddressDataSource addressDataSource = new AddressDataSource(context);
			addressDataSource.write();;
			address = addressDataSource.getCurrentAddress();
			if(address != null) {
				GeneralDefs.currentSelectedAddress = address;
			}
			addressDataSource.close();
		}
		return address;
	}

	public static void updateSavedAddress(Address address, Context context) {
		if(address != null) {
			AddressDataSource addressDataSource = new AddressDataSource(context);
			addressDataSource.write();;
			addressDataSource.updateAddress(address.getId(), address.getAddressPrimary(),
					address.getLandmark(), address.getMobileNumber());
			addressDataSource.close();
			GeneralDefs.currentSelectedAddress = address;
		}
	}

	public static int getImageResource(String category, Context context) {
		CategoryDataSource categoryDataSource = new CategoryDataSource(context);
		if(category.equals("Vehicle Maintainance")) {
			category = "Mechanic";
		}
		int resourceId = 0;
		try {
			categoryDataSource.read();
			resourceId = categoryDataSource.getImageResource(category);
			categoryDataSource.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resourceId;
	}

	public static boolean getSharedPreferencesBoolean(Context context, String key) {
		SharedPreferences sharedPreferences =
				PreferenceManager.getDefaultSharedPreferences(context);
		boolean boolValue = sharedPreferences
				.getBoolean(key, false);
		return boolValue;
	}

	public static AppConfig getAppConfig(Context context) {
		AppConfig  appConfig = GeneralDefs.appConfig;
		if(appConfig == null) {
			String filename = GeneralDefs.APP_CONFIG_FILE;
			FileInputStream inputStream = null;
			InputStreamReader inputStreamReader = null;
			BufferedReader reader = null;
			StringBuilder total = new StringBuilder();
			boolean isFreshConfigAvailable = false;
//			try {
//				isFreshConfigAvailable = context.getFileStreamPath(filename).exists();
//			} catch (Exception e) { e.printStackTrace(); }
			try{
//				if(isFreshConfigAvailable) {
//					inputStream = context.openFileInput(filename);
//					inputStream.read();
//					inputStreamReader = new InputStreamReader(inputStream);
//					Logger.z("Utils", "Reading from  fresh file copy");
//
//
//				} else {
					inputStreamReader = new InputStreamReader(context.getAssets().open("config/"+GeneralDefs.APP_CONFIG_FILE));
					Logger.z("Utils", "Unable to read from fresh file copy");
//				}
				reader = new BufferedReader(inputStreamReader);
				String mLine;
				while ((mLine = reader.readLine()) != null) {
					total.append(mLine);
				}
				Logger.z("File Content", total.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				try {
					if (reader != null) {
						reader.close();
					}
					if(inputStream != null) {
						inputStream.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			appConfig = new AppConfig(total.toString());
			GeneralDefs.appConfig = appConfig;
		}
		return appConfig;
	}


}
