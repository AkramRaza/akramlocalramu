package com.localramu.android.app.background;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.android.app.utils.MD5HashGenerator;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.UserType;
import com.localramu.common.messages.service.request.LoginRequest;
import com.localramu.common.messages.service.response.LoginResponse;
import com.localramu.common.url.UrlDefs;


public class LoginProcess {

	private String username, password;
	private Context context;
	private UserInfoDataSource userInfoDataSource;

	public LoginProcess(String username, String password, Context context) {
		this.username = username;
		this.password = password;
		this.context = context;
		this.userInfoDataSource = new UserInfoDataSource(context);
	}

	public StatusCode login() {
		StatusCode statusCode = StatusCode.UNKNOWN;;
		try{
			MD5HashGenerator md5HashGenerator = new MD5HashGenerator();
			String passwordHash = md5HashGenerator.generatHash(password);
			
			String deviceUserId = Secure.getString(context.getContentResolver(),
					Secure.ANDROID_ID); 
			RestTemplate restTemplate = new RestTemplate();
			LoginRequest loginRequest = new LoginRequest();
			loginRequest.setUsername(username);
			loginRequest.setPassword(passwordHash);
			loginRequest.setDeviceUserId(deviceUserId);
			loginRequest.setUserType(UserType.CUSTOMER);
			
			restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
			JSONObject json = new JSONObject();
			Gson gson = new Gson();

			
			Log.i("Request",  gson.toJson(loginRequest));
			LoginResponse response = restTemplate.postForObject(UrlDefs.LOGIN_NEW_URL,
					loginRequest, LoginResponse.class);
			Log.i("Response",  gson.toJson(response));

			statusCode = response.getStatusCode();
			if(response.isSuccess()) {
				userInfoDataSource.write();
				userInfoDataSource.logoutAllUser();
				userInfoDataSource.loginForExistingUser(response.getUserInfo(), "STANDARD");
				userInfoDataSource.close();
			}
			Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());

		}
		catch(Exception e){
			e.printStackTrace();
			statusCode = StatusCode.ERROR_OCCURRED;
		}
		return statusCode;

	}
}
