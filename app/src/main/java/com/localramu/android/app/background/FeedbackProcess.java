package com.localramu.android.app.background;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.android.app.utils.MD5HashGenerator;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.UserType;
import com.localramu.common.messages.model.Job;
import com.localramu.common.messages.model.UserInfo;
import com.localramu.common.messages.service.request.FeedbackRequest;
import com.localramu.common.messages.service.request.LoginRequest;
import com.localramu.common.messages.service.response.FeedbackResponse;
import com.localramu.common.messages.service.response.LoginResponse;
import com.localramu.common.url.UrlDefs;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by mohitgupta on 31/07/16.
 */
public class FeedbackProcess {
    private String feedback, rating;
    private Context context;
    private Job job;

    public FeedbackProcess(String feedback, String rating, Job job, Context context) {
        this.feedback = feedback;
        this.rating = rating;
        this.context = context;
        this.job = job;
    }

    public StatusCode sendFeedback() {
        StatusCode statusCode = StatusCode.UNKNOWN;;
        try{

            String deviceUserId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            UserInfo userInfo = Utilities.getUserInfo(context);
            RestTemplate restTemplate = new RestTemplate();
            FeedbackRequest feedbackRequest = new FeedbackRequest();
            feedbackRequest.setRequestingDevice(deviceUserId);
            feedbackRequest.setRequestingUser(userInfo.getUserId());
            feedbackRequest.setFeedback(feedback);
            feedbackRequest.setRating(rating);

            if(job != null) {
                feedbackRequest.setJobId(job.getJobId());
                feedbackRequest.setRequestId(job.getCustomerId());
            }
            feedbackRequest.setRequestingDevice(deviceUserId);
            restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());

            Gson gson = new Gson();
            Log.i("Request", gson.toJson(feedbackRequest));
            FeedbackResponse response = restTemplate.postForObject(UrlDefs.FEEDBACK_URL,
                    feedbackRequest, FeedbackResponse.class);
            Log.i("Response",  gson.toJson(response));

            statusCode = response.getStatusCode();
            if(response.isSuccess()) {
               statusCode = StatusCode.SUCCESS;
            }
            Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());

        }
        catch(Exception e){
            e.printStackTrace();
            statusCode = StatusCode.ERROR_OCCURRED;
        }
        return statusCode;

    }
}
