package com.localramu.android.app;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.localramu.android.app.adapter.ServicePartnerListAdapter;
import com.localramu.android.app.background.GetServiceListProcess;
import com.localramu.android.app.background.SignupProcess;
import com.localramu.android.app.defs.GeneralDefs;
import com.localramu.android.app.models.Address;
import com.localramu.android.app.ui.CustomToast;
import com.localramu.android.app.ui.RecyclerItemClickListener;
import com.localramu.android.app.utils.DistanceCalculator;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.model.BasicInfo;
import com.localramu.common.messages.model.Service;
import com.localramu.common.messages.model.ServiceInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ServicePartnerActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String category;
    private ProgressDialog progressDialog;
    private ObjectAnimator animation;
    private DistanceCalculator distanceCalculator = new DistanceCalculator();
    Address address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        category = intent.getStringExtra("SELECTED_CATEGORY");
        setContentView(R.layout.activity_service_partner);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //address = Utilities.getSelectedAddress(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(category);

        showProgress();

        mapResource();

        address = Utilities.getSelectedAddress(getApplicationContext());

        if(address != null) {

            new GetServiceListTask(category, address.getLatitude(), address.getLongitude()).execute();
        } else {
            showAddressSearchActivity();
        }
    }

    private void mapResource () {

        mRecyclerView = (RecyclerView) findViewById(R.id.service_detail_list);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(),
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                view.getTag();
                            }
                        })
        );
    }

    /**
     * Represents an asynchronous to get the list of Services
     */
/*
    class ServiceComarator implements Comparator<Service> {

        @Override
        public int compare(Service lhs, Service rhs) {

            double dist1 = distanceCalculator.distance(address.getLatitude(), address.getLongitude(), lhs.getLatitude(),
                    rhs.getLongitude(), "K");

            double dist2 = distanceCalculator.distance(address.getLatitude(), address.getLongitude(), rhs.getLatitude(),
                    rhs.getLongitude(), "K");
            *//*if (dist1 > dist2) {
                return 1;
            } else {
                return -1;\;lkjfdsa
            }
*//*
            return Double.compare(dist1, dist2);

        }
    }*/
    public class GetServiceListTask extends AsyncTask<Void, Void, List<Service>> {

        private final String mCategory;
        private final double mLatitude;
        private final double mLongitude;


        GetServiceListTask(String category, double latitude, double longitude) {
            mCategory = category;
            mLatitude = latitude;
            mLongitude= longitude;
        }

        @Override
        protected void onPreExecute() {
        }
        @Override
        protected List<Service> doInBackground(Void... params) {
            GetServiceListProcess getServiceListProcess = new GetServiceListProcess(mCategory, mLatitude, mLongitude,
                    getApplicationContext());
            return getServiceListProcess.getServices();
        }

        @Override
        protected void onPostExecute(List<Service> serviceList) {

            List<Service> serviceListTemp = serviceList;
            for (Service service : serviceListTemp)
            {
                double dist1 = distanceCalculator.distance(address.getLatitude(), address.getLongitude(), service.getLatitude(),
                        service.getLongitude(), "K");
                Log.d("service111",""+dist1);
            }

            if(serviceListTemp != null && serviceListTemp.size() != 0) {
                Collections.sort(serviceListTemp, new Comparator<Service>() {

                    @Override
                    public int compare(Service lhs, Service rhs) {
                        double dist1 = distanceCalculator.distance(address.getLatitude(), address.getLongitude(), lhs.getLatitude(),

                                lhs.getLongitude(), "K");

                        double dist2 = distanceCalculator.distance(address.getLatitude(), address.getLongitude(), rhs.getLatitude(),
                                rhs.getLongitude(), "K");


                        Log.d("dist1dist2",""+dist1);

                        Log.d("dist2dist2",""+dist2);
                            /*if (dist1 > dist2) {
                                return 1;
                            } else {
                                return -1;\;lkjfdsa
                            }*/

                        //return dist1.compareTo(dist2);
                        return Double.compare(dist1, dist2);

                    }
                });

                for (Service service : serviceListTemp)
                {
                    double dist1 = distanceCalculator.distance(address.getLatitude(), address.getLongitude(), service.getLatitude(),
                            service.getLongitude(), "K");
                    Log.d("service",""+dist1);
                }




                // specify an adapter (see also next example)
                mAdapter = new ServicePartnerListAdapter(serviceListTemp,getApplicationContext(), ServicePartnerActivity.this);
                mRecyclerView.setAdapter(mAdapter);
            } else {
                ServicePartnerActivity.this.findViewById(R.id.error_detail_panel).setVisibility(View.VISIBLE);
            }
            cancelProgress();
        }

        @Override
        protected void onCancelled() {
            cancelProgress();
        }
    }

    private void showProgress() {
        // Locate view
        TextView ring = (TextView) findViewById(R.id.ring);

        animation = ObjectAnimator.ofFloat(ring, "rotationY", 0.0f, 360f);
        animation.setDuration(2000);
        animation.setRepeatCount(ObjectAnimator.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        animation.start();
    }

    private void cancelProgress() {
        if(animation != null) {
            animation.end();
        }
        ((RelativeLayout) findViewById(R.id.custom_progress)).setVisibility(View.GONE);
    }

    public void showAddressSearchActivity() {
        Intent intent = new Intent(getApplicationContext(),AddressSearchActivity.class);
        startActivity(intent);
    }

    public void tryDifferentLocation(View view) {
        showAddressSearchActivity();
        finish();
    }

    public void tryExpressBooking(View view) {
        Intent intent = new Intent(getApplicationContext(),ExpressBookingActivity.class);
        startActivity(intent);
        finish();
    }
}
