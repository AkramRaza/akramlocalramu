package com.localramu.android.app.background;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.GetServiceRequestType;
import com.localramu.common.messages.model.Offer;
import com.localramu.common.messages.model.Service;
import com.localramu.common.messages.service.request.GetOffersRequest;
import com.localramu.common.messages.service.request.GetServiceListRequest;
import com.localramu.common.messages.service.response.GetOffersResponse;
import com.localramu.common.messages.service.response.GetServiceListResponse;
import com.localramu.common.url.UrlDefs;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by mohitgupta on 21/03/16.
 */
public class GetOffersProcess {

    private Context context;

    public GetOffersProcess( Context context) {
        this.context = context;
    }

    public List<Offer> getOffers() {
        List<Offer> offers = null;
        StatusCode statusCode = StatusCode.UNKNOWN;
        try{


            String deviceUserId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            RestTemplate restTemplate = new RestTemplate();
            GetOffersRequest getOffersRequest = new GetOffersRequest();

            restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
            JSONObject json = new JSONObject();
            Gson gson = new Gson();


            Log.i("Request", gson.toJson(getOffersRequest));
            GetOffersResponse response = restTemplate.postForObject(UrlDefs.GET_OFFER_URL,
                    getOffersRequest, GetOffersResponse.class);
            statusCode = response.getStatusCode();
            if(response.isSuccess()) {
                offers = response.getOffers();
                         }
            Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());

        }
        catch(Exception e){
            e.printStackTrace();
            statusCode = StatusCode.ERROR_OCCURRED;
        }
        return offers;

    }
}
