package com.localramu.android.app.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/**
 * Created by mohitgupta on 20/02/16.
 */

public class InternetDetector {

    private Context mContext;

    private final String TAG = "InternetDetector";
    public InternetDetector(Context context){
        this.mContext = context;
    }

    public boolean isConnectingToInternet(){
        boolean isConnected = false;
        try{
            ConnectivityManager cm =
                    (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
            Logger.z(TAG, "Internet Connection status - isConnected "+isConnected);

        }
        catch(Exception e) {
            Logger.z(6, TAG, "InternetUpdateReciever Down!!");
            return true;
        }
        return true;
    }
}
