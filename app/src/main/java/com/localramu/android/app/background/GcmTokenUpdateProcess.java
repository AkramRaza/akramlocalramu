package com.localramu.android.app.background;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.UserInfoUpdateType;
import com.localramu.common.messages.model.UserInfo;
import com.localramu.common.messages.service.request.UpdateUserInfoRequest;
import com.localramu.common.messages.service.response.UpdateUserInforResponse;
import com.localramu.common.url.UrlDefs;

public class GcmTokenUpdateProcess {

	private UserInfo userInfo;
	private Context context;
	private UserInfoDataSource userInfoDataSource;

	public GcmTokenUpdateProcess(UserInfo userInfo, Context context) {
		this.userInfo = userInfo;
		this.context = context;
		this.userInfoDataSource = new UserInfoDataSource(context);
	}

	public StatusCode updateToken() {
		StatusCode statusCode = StatusCode.UNKNOWN;;
		try{
			if(userInfo.getDeviceId() == null || userInfo.getDeviceId().equals("")) {
				String deviceUserId = Secure.getString(context.getContentResolver(),
						Secure.ANDROID_ID); 
				userInfo.setDeviceId(deviceUserId);
			}
			
			RestTemplate restTemplate = new RestTemplate();
			UpdateUserInfoRequest updateUserInfoRequest = new UpdateUserInfoRequest();
			updateUserInfoRequest.setUserInfo(userInfo);
			updateUserInfoRequest.setUserInfoUpdateType(UserInfoUpdateType.GCM_UPDATE);
			
			restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
			JSONObject json = new JSONObject();
			Gson gson = new Gson();

			
			Log.i("Request",  gson.toJson(updateUserInfoRequest));
			UpdateUserInforResponse response = restTemplate.postForObject(UrlDefs.USER_UPDATE_URL, 
					updateUserInfoRequest, UpdateUserInforResponse.class);
			if(response.isSuccess()) {
				statusCode = StatusCode.SUCCESS;
				userInfoDataSource.write();
				userInfoDataSource.updateGcmInfo(userInfo.getUserId(), userInfo.getDeviceId(), userInfo.getGcmId(), true);
				userInfoDataSource.close();
			} else {
				statusCode = StatusCode.FAILURE;
			}
			Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());

		}
		catch(Exception e){
			e.printStackTrace();
			statusCode = StatusCode.ERROR_OCCURRED;
		}
		return statusCode;

	}
}
