package com.localramu.android.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.localramu.android.app.background.GetServiceProcess;
import com.localramu.android.app.fonts.TextViewNormalFont;
import com.localramu.android.app.models.Address;
import com.localramu.android.app.utils.DistanceCalculator;
import com.localramu.android.app.utils.TimeUtils;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.model.AddressInfo;
import com.localramu.common.messages.model.BasicInfo;
import com.localramu.common.messages.model.Service;
import com.localramu.common.messages.model.ServiceInfo;

import java.util.Map;

public class ServiceDetailActivity extends AppCompatActivity {

    private Service service;
    private ProgressDialog progressDialog;
    public GoogleMap googleMap;
    private ScrollView scrollView;
    private FloatingActionButton callButton, mapButton, serviceRequestButton;
    private String vendorId;
    private Service mService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);
        Intent intent = getIntent();
        String serviceId = intent.getStringExtra("SERVICE_ID");
        vendorId = intent.getStringExtra("VENDOR_ID");
        String displayName = intent.getStringExtra("DISPLAY_NAME");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(displayName != null) {
            getSupportActionBar().setTitle(displayName);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mapResource();
        new GetServiceTask(serviceId, vendorId).execute();
        initilizeMap();
    }

    private void mapResource () {
        scrollView = (ScrollView) findViewById(R.id.service_detail_page);

        callButton = (FloatingActionButton) findViewById(R.id.call_button);
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:8792341573"));
                startActivity(intent);
            }
        });


        mapButton = (FloatingActionButton) findViewById(R.id.map_button);
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusOnMap();
            }
        });

        serviceRequestButton = (FloatingActionButton) findViewById(R.id.job_request_button);

        serviceRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),JobBookingActivity.class);
                intent.putExtra("SERVICE_ID", mService.getServiceId());
                intent.putExtra("VENDOR_USER_ID", mService.getVendorUserId());
                intent.putExtra("VISITING_CHARGE", "100");
                intent.putExtra("CATEGORY", mService.getServiceInfo().getCategory());
                intent.putExtra("DISPLAY_NAME", mService.getBasicInfo().getDisplayName());
                intent.putExtra("FULL_NAME", mService.getBasicInfo().getFullName());


                startActivity(intent);
            }
        });

        progressDialog = ProgressDialog.show(this, "Please wait ...", "Fetching Services ...", true);
        progressDialog.setCancelable(false);

    }

    private void setLocation(double latitude, double longitude) {
        if(googleMap != null) {
            setMarkerOnMap(new LatLng(latitude, longitude));
        } else {
            initilizeMap();
        }
    }

    private void setMarkerOnMap(LatLng latLng) {
        if(googleMap != null) {
            googleMap.addMarker(new MarkerOptions()
                    .position(latLng));
            CameraPosition cameraPosition =  new CameraPosition.Builder().target(latLng)
                    .zoom(15.5f)
                    .bearing(0)
                    .tilt(25)
                    .build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }
    private void displayServiceDetails(Service service) {
        if(service != null) {
            BasicInfo basicInfo = service.getBasicInfo();
            ServiceInfo serviceInfo = service.getServiceInfo();

            if(basicInfo != null ) {
                if (basicInfo.getAddress() != null) {
                    AddressInfo addressInfo = basicInfo.getAddress();
                    String address = String.format("%s,%s", addressInfo.getAddressLine1(),
                            addressInfo.getAddressLine2());
                    ((TextView) findViewById(R.id.address)).setText(address);
                }
                ((TextView) findViewById(R.id.display_name)).setText(basicInfo.getDisplayName());
                ((TextView) findViewById(R.id.full_name)).setText(basicInfo.getFullName());
            }

            if(serviceInfo != null ) {
                ((TextView) findViewById(R.id.service_experience)).setText(serviceInfo.getServiceExperience());
                ((TextView) findViewById(R.id.visiting_price)).setText(serviceInfo.getVisitingCost());

                ((TextView) findViewById(R.id.about_vendor)).setText(serviceInfo.getAboutVendor());
                ((TextView) findViewById(R.id.language_known)).setText(String.valueOf(serviceInfo.getLanguageKnown()));

                ((TextView) findViewById(R.id.working_days)).setText(TimeUtils.convertDaysToString(serviceInfo.getWorkingDays()));

                ((TextView) findViewById(R.id.working_time)).setText(TimeUtils.convertTime(serviceInfo.getWorkingTimeFrom(), serviceInfo.getWorkingTimeTo()));

                if(serviceInfo.isPriorBookingRequired()) {
                    ((TextView) findViewById(R.id.prior_booking_time)).setText(String.format("%s Hours", serviceInfo.getPriorBookingTime()));
                } else {
                    findViewById(R.id.prior_booking_time_row).setVisibility(View.GONE);
                }

                if(serviceInfo.getPriceChart() != null && serviceInfo.getPriceChart().size() != 0) {
                    populatePriceTable(serviceInfo.getPriceChart());
                } else {
                    findViewById(R.id.price_chart_card).setVisibility(View.GONE);
                }
            }

            int resourceId = Utilities.getImageResource(serviceInfo.getCategory(), getApplicationContext());

            if(resourceId == 0) {
                resourceId = R.drawable.logo;
            }
            ((ImageView) findViewById(R.id.service_partner_image)).setBackgroundResource(resourceId);


            Address address = Utilities.getSelectedAddress(getApplicationContext());
            DistanceCalculator distanceCalculator = new DistanceCalculator();
            if(address != null) {
                double distance = distanceCalculator.distance(address.getLatitude(), address.getLongitude(), service.getLatitude(),
                        service.getLongitude(),"K");
                ((TextView) findViewById(R.id.distance)).setText(String.format("%.2f Km away from you", distance));
            }

            setLocation(service.getLatitude(), service.getLongitude());
        }
    }

    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
        if (googleMap == null) {
            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap map) {
                    googleMap = map;
                    googleMap.getUiSettings().setZoomControlsEnabled(false);
                    googleMap.getUiSettings().setCompassEnabled(false);
                    if (service != null) {
                        setMarkerOnMap(new LatLng(service.getLatitude(), service.getLongitude()));
                    }
                }
            });
        }
    }

    private void populatePriceTable(Map<String, Integer> priceChartMap) {


        TableLayout priceChartTable = (TableLayout) findViewById(R.id.price_chart);

        if(priceChartMap != null && priceChartMap.size() != 0) {
            for(Map.Entry<String, Integer> entry : priceChartMap.entrySet()) {
                TableRow table_row = new TableRow(this);

                TableRow.LayoutParams layoutParams= new TableRow.LayoutParams(
                        TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT);

                layoutParams.setMargins(10,10,10,10);
                table_row.setLayoutParams(layoutParams);

                TextViewNormalFont service = new TextViewNormalFont(this);
                TextViewNormalFont price = new TextViewNormalFont(this);

                service.setText(entry.getKey());
                price.setText(String.valueOf(entry.getValue()));
                service.setTextSize(16);
                price.setTextSize(16);
                price.setGravity(Gravity.RIGHT);

                service.setPadding(10, 10, 10, 10);
                price.setPadding(10,10,10,10);
                table_row.addView(service);
                table_row.addView(price);

                priceChartTable.addView(table_row);
            }
        }
    }

    private final void focusOnMap(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, findViewById(R.id.map_canvas_card).getTop());
            }
        });
    }

    /**
     * Represents an asynchronous to get the list of Services
     */
    public class GetServiceTask extends AsyncTask<Void, Void, Service> {

        private final String mServiceId;
        private final String mVendorUserId;


        GetServiceTask(String serviceId, String vendorUserId) {
            mServiceId = serviceId;
            mVendorUserId = vendorUserId;
        }

        @Override
        protected Service doInBackground(Void... params) {
            GetServiceProcess getServiceProcess = new GetServiceProcess(mServiceId, mVendorUserId,
                    getApplicationContext());
            return getServiceProcess.getServices();
        }

        @Override
        protected void onPostExecute(final Service service) {
            if(service != null) {
                mService = service;
                displayServiceDetails(service);
            }
            try {
                if ((progressDialog != null) && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                // Handle or log or ignore
            } catch (final Exception e) {
                // Handle or log or ignore
            } finally {
                progressDialog = null;
            }

        }

        @Override
        protected void onCancelled() {
            try {
                if ((progressDialog != null) && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (final IllegalArgumentException e) {
                // Handle or log or ignore
            } catch (final Exception e) {
                // Handle or log or ignore
            } finally {
                progressDialog = null;
            }
        }
    }

}
