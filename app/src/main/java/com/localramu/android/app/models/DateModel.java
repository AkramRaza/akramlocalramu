package com.localramu.android.app.models;

import java.util.Date;

/**
 * Created by mohitgupta on 05/02/16.
 */
public class DateModel {
    private String dateString;
    private Date dateObject;

    public DateModel(String dateString, Date dateObject) {
        this.dateObject = dateObject;
        this.dateString = dateString;
    }


    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public Date getDateObject() {
        return dateObject;
    }

    public void setDateObject(Date dateObject) {
        this.dateObject = dateObject;
    }
}
