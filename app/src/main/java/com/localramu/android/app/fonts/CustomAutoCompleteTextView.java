package com.localramu.android.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.AutoCompleteTextView;

/**
 * Created by mohitgupta on 07/02/16.
 */
public class CustomAutoCompleteTextView extends AutoCompleteTextView{

    public CustomAutoCompleteTextView(Context context)
    {
        super(context);
        setFont();
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setFont();
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setFont();
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs)
    {
        InputConnection conn = super.onCreateInputConnection(outAttrs);
        outAttrs.imeOptions &= ~EditorInfo.IME_FLAG_NO_ENTER_ACTION;
        return conn;
    }

    private void setFont() {
        Typeface normal = Typeface.createFromAsset(getContext().getAssets(),"fonts/MuseoSansRounded/MuseoSansRounded-300.otf");
        setTypeface( normal, Typeface.NORMAL );

    }
}
