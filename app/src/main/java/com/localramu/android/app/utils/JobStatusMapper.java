package com.localramu.android.app.utils;

import com.localramu.common.messages.codes.ExternalJobStatus;
import com.localramu.common.messages.codes.JobStatus;

/**
 * Created by mohitgupta on 09/03/16.
 */
public class JobStatusMapper {

    public static ExternalJobStatus findExternalJobStatus(JobStatus jobStatus) {
        ExternalJobStatus externalJobStatus = null;
        switch (jobStatus) {
            case FLIGHT:
                externalJobStatus = ExternalJobStatus.PROCESSING;
                break;
            case ACCEPTED:
            case IN_PROGRESS:
            case WITHDRAWN:
            case AVAILABLE:
                externalJobStatus = ExternalJobStatus.ACCEPTED;
                break;
            case COMPLETED:
                externalJobStatus = ExternalJobStatus.COMPLETED;
                break;
            case CANCELLED:
                externalJobStatus = ExternalJobStatus.CANCELLED;
                break;
        }
        return externalJobStatus;
    }
}
