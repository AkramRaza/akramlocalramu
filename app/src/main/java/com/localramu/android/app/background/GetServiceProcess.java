package com.localramu.android.app.background;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.localramu.android.app.db.UserInfoDataSource;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.GetServiceRequestType;
import com.localramu.common.messages.model.Service;
import com.localramu.common.messages.service.request.GetServiceListRequest;
import com.localramu.common.messages.service.response.GetServiceListResponse;
import com.localramu.common.url.UrlDefs;

import org.json.JSONObject;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by mohitgupta on 14/02/16.
 */
public class GetServiceProcess {

    private String serviceId;
    private String vendorId;
    private Context context;

    public GetServiceProcess(String serviceId, String vendorId, Context context) {
        this.serviceId = serviceId;
        this.vendorId = vendorId;
        this.context = context;
    }

    public Service  getServices() {
        Service service = null;
        StatusCode statusCode = StatusCode.UNKNOWN;
        try{

            String deviceUserId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            RestTemplate restTemplate = new RestTemplate();
            GetServiceListRequest getServiceListRequest = new GetServiceListRequest();
            getServiceListRequest.setServiceId(serviceId);
            getServiceListRequest.setVedorUserId(vendorId);
            getServiceListRequest.setGetServiceRequestType(GetServiceRequestType.SIMPLE);


            restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
            JSONObject json = new JSONObject();
            Gson gson = new Gson();


            Log.i("Request", gson.toJson(getServiceListRequest));
            GetServiceListResponse response = restTemplate.postForObject(UrlDefs.GET_SERVICE_URL,
                    getServiceListRequest, GetServiceListResponse.class);
            statusCode = response.getStatusCode();
            if(response.isSuccess()) {
                service = response.getService();
            }
            Log.i("Httppost created :",response.getStatusCode()+","+response.isSuccess());

        }
        catch(Exception e){
            e.printStackTrace();
            statusCode = StatusCode.ERROR_OCCURRED;
        }
        return service;

    }
}
