package com.localramu.android.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.localramu.android.app.background.JobRequestProcess;
import com.localramu.android.app.db.CategoryDataSource;
import com.localramu.android.app.defs.GeneralDefs;
import com.localramu.android.app.defs.TableDefs;
import com.localramu.android.app.models.Address;
import com.localramu.android.app.models.DateTimeAdapterData;
import com.localramu.android.app.models.OfferSettingModel;
import com.localramu.android.app.models.TimeRangeModel;
import com.localramu.android.app.models.WaterPriceModel;
import com.localramu.android.app.ui.CustomToast;
import com.localramu.android.app.utils.AppConfig;
import com.localramu.android.app.utils.InternetDetector;
import com.localramu.android.app.utils.JobFactory;
import com.localramu.android.app.utils.Utilities;
import com.localramu.common.messages.codes.JobExtraParamKeys;
import com.localramu.common.messages.codes.StatusCode;
import com.localramu.common.messages.enums.JobType;
import com.localramu.common.messages.model.Job;
import com.localramu.common.messages.model.UserInfo;


import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class ExpressBookingActivity extends AppCompatActivity {

    private CategoryDataSource categoryDataSource;
    private Spinner categorySpinner, dateSpinner, timeRangeSpinner, deliveryTime;
    private DateTimeAdapterData dateTimeAdapterData = DateTimeAdapterData.getRangeInstance();
    TextView categoryName;
    private JobFactory jobFactory = new JobFactory();
    private InternetDetector internetDetector;

    static final int ADDRESS_FIND_REQUEST = 1;  // The request code

    private String selectedCategory = null;
    private JobType jobType ;

    private int dateRangeEnabledIndex = 0, timeRangeEnabledIndex = 0;
    private boolean isBookingOnTodaysDate = true;
    ArrayAdapter<String> timeRangeArrayAdapter;

    String LOCALRAMUPREF = "LocalRamuPref" ;


    private List<WaterPriceModel> waterPrices = null;
    private AppConfig appConfig = null;

    public ExpressBookingActivity() {
        //categoryDataSource = new CategoryDataSource(getApplicationContext());
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_express_booking);
        Intent intent = getIntent();
        selectedCategory = intent.getStringExtra("SELECTED_CATEGORY");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SharedPreferences sharedpreferences = getSharedPreferences(LOCALRAMUPREF, Context.MODE_PRIVATE);
        EditText mobile = (EditText)findViewById(R.id.mobile);
        EditText address = (EditText)findViewById(R.id.address);
        EditText landmark = (EditText)findViewById(R.id.landmark);
        categoryName = (TextView) findViewById(R.id.categoryName);
        categorySpinner = (Spinner) findViewById(R.id.category_spinner);
        sharedpreferences = getSharedPreferences(LOCALRAMUPREF, Context.MODE_PRIVATE);
        if(sharedpreferences.getString("mobileExpress","")!=null){
            mobile.setText(sharedpreferences.getString("mobileExpress",""));
            Log.e("&&&&&&&&&&&",sharedpreferences.getString("mobileExpress",""));
        }if(sharedpreferences.getString("houseExpress","")!=null){
            address.setText(sharedpreferences.getString("houseExpress",""));
            Log.e("&&&&&&&&&&&",sharedpreferences.getString("houseExpress",""));
        }if(sharedpreferences.getString("landmarkExpress","")!=null){
            landmark.setText(sharedpreferences.getString("landmarkExpress",""));
            Log.e("&&&&&&&&&&&",sharedpreferences.getString("landmarkExpress",""));
        }

        Bundle bndl= getIntent().getExtras();
        if(bndl!=null) {
            int categoryNo = bndl.getInt("categoryNo");
            if (categoryNo == 0) {
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Water Can");
            }else if(categoryNo == 1){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Carpenter");

            }
            else if(categoryNo == 2){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Plumber");


            }
            else if(categoryNo == 3){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Electrician");


            }
            else if(categoryNo == 4){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Laundry");


            }
            else if(categoryNo == 5){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Mechanic");


            }
            else if(categoryNo == 6){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Beautician");


            }
            else if(categoryNo == 7){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Internet & DTH");


            }
            else if(categoryNo == 8){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Documents");


            }
            else if(categoryNo == 9){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Electronics");


            }
            else if(categoryNo == 10){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Photographer");

            }
            else if(categoryNo == 11){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Ladies Tailor");

            }else if(categoryNo == 12){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("House Painting");

            }
            else if(categoryNo == 13){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Puja Pandit");

            }else if(categoryNo == 14){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Yoga Teacher");


            }else if(categoryNo == 15){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Water Tanker");


            }else if(categoryNo == 16){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Movers and Packers");


            }else if(categoryNo == 17){
                categorySpinner.setVisibility(View.GONE);
                categoryName.setVisibility(View.VISIBLE);
                categoryName.setText("Pest Control");
            }else{
                Snackbar.make(getWindow().getDecorView(),"We are Adding more categories shortly",Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }
        }

        mobile.setText(sharedpreferences.getString("mobilenoKey",""));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        categoryDataSource = new CategoryDataSource(getApplicationContext());
        mapViews();
        setAdapter();
        internetDetector = new InternetDetector(getApplicationContext());
        appConfig = Utilities.getAppConfig(getApplicationContext());

        if(selectedCategory != null) {
            switch(selectedCategory) {
                case "Water Can" :
                    jobType = JobType.WATER_CAN_DELIVERY;
                    if(appConfig != null) {
                        displayOffer(appConfig.getWaterCanOfferSetting());
                    }
                    handleWaterCan();
                    break;
                case "Laundry":
                    jobType = JobType.LAUNDARY;
                    handleLaundry();
                default:
            }
        } else {
            jobType = JobType.HOME_SERVICE;
        }

        populateData();
    }

    private void mapViews() {

        dateSpinner = (Spinner) findViewById(R.id.date);
        timeRangeSpinner = (Spinner) findViewById(R.id.time_range);
        deliveryTime = (Spinner) findViewById(R.id.when);
        Address address = Utilities.getSelectedAddress(getApplicationContext());
        if(address != null) {
            EditText addressLine2 = (EditText) findViewById(R.id.locality);
            addressLine2.setText(address.getAddressSecondary());
            addressLine2.setEnabled(false);
        }


        deliveryTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                int item = deliveryTime.getSelectedItemPosition();
                switch (item) {
                    case 1:
                        ((CardView) findViewById(R.id.date_panel)).setVisibility(View.VISIBLE);
                        ((CardView) findViewById(R.id.time_range_panel)).setVisibility(View.VISIBLE);
                        if (dateRangeEnabledIndex == 0) {
                            timeRangeSpinner.setSelection(timeRangeEnabledIndex);
                        }
                        break;
                    case 0:
                        ((CardView) findViewById(R.id.date_panel)).setVisibility(View.GONE);
                        ((CardView) findViewById(R.id.time_range_panel)).setVisibility(View.GONE);
                }

            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });






    }
    private void setAdapter() {
        ArrayList<String> categoryList = new ArrayList<>();
        try {
            categoryDataSource.read();
            Cursor cursor = categoryDataSource.getCategoryListWithNormalMenuPath();
            if(cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    categoryList.add(cursor.getString(cursor.getColumnIndex(TableDefs.COLUMN_CATEGORY_NAME)));
                    cursor.moveToNext();
                }
            }
            categoryDataSource.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, categoryList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(spinnerArrayAdapter);

        ArrayAdapter<String> dateArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, dateTimeAdapterData.getDateStringList()) {
            // Disable click item < month current
            @Override
            public boolean isEnabled(int position) {
                int currentHour = getCurrentHour();
                if (position == 0 && !isValidBookingTimeForToday()) {
                    dateRangeEnabledIndex = 1;
                    return false;
                }
                return true;
            }
            // Change color item
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                // TODO Auto-generated method stub
                View mView = super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                int currentHour = getCurrentHour();
                if (position == 0 && !isValidBookingTimeForToday()) {
                    mTextView.setTextColor(Color.GRAY);

                }
                return mView;
            }
        };
        dateArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateSpinner.setAdapter(dateArrayAdapter);


        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                int item = dateSpinner.getSelectedItemPosition();
                timeRangeSpinner.setAdapter(null);
                timeRangeSpinner.invalidate();
                if (item == 0) {
                    timeRangeSpinner.setAdapter(setTimeRangeAdapter(true));
                } else {
                    timeRangeSpinner.setAdapter(setTimeRangeAdapter(false));
                }
                //timeRangeSpinner.setSelection(timeRangeEnabledIndex);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        timeRangeSpinner.setAdapter(setTimeRangeAdapter(true));
        //timeRangeSpinner.setSelection(timeRangeEnabledIndex);

    }

    private ArrayAdapter<String> setTimeRangeAdapter(final boolean isTodaysDate) {
        timeRangeEnabledIndex = 0;
        ArrayAdapter<String> arrayAdapter =  new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, dateTimeAdapterData.getTimeStringList()) {
            // Disable click item < month current
            @Override
            public boolean isEnabled(int position) {
                int currentHour = getCurrentHour();
                TimeRangeModel timeRangeModel = dateTimeAdapterData.getTimeRange(position);
                if (timeRangeModel != null && isTodaysDate && currentHour > timeRangeModel.getFromTimeInt()) {
                    timeRangeEnabledIndex = position + 1;
                    return false;
                }
                return true;
            }
            // Change color item
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                // TODO Auto-generated method stub
                View mView = super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                int currentHour = getCurrentHour();
                TimeRangeModel timeRangeModel = dateTimeAdapterData.getTimeRange(position);
                if (timeRangeModel != null && isTodaysDate && currentHour > timeRangeModel.getFromTimeInt()) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return arrayAdapter;
    }


    private void populateData() {
        EditText locality = (EditText) findViewById(R.id.locality);
        Address address = Utilities.getSelectedAddress(getApplicationContext());
        if(address != null) {
            locality.setText(address.getAddressSecondary());
            locality.setEnabled(true);
            if(address.getMobileNumber() != null) {
                ((EditText) findViewById(R.id.address)).setText(address.getAddressPrimary());
                ((EditText) findViewById(R.id.landmark)).setText(address.getLandmark());
                ((EditText) findViewById(R.id.mobile)).setText(address.getMobileNumber());
            }
        }

        int currentHour = getCurrentHour();
        if(!isValidBookingTimeForToday()) {
            disableBookNowOption();
            dateSpinner.setSelection(1);
        } else if(timeRangeEnabledIndex < dateTimeAdapterData.getTimeRangeSize()) {
            timeRangeSpinner.setSelection(timeRangeEnabledIndex);
        }

    }

    /**
     * Represents an asynchronous Job Creation Request task used tocreate new job
     * the user.
     */
    public class JobRequestTask extends AsyncTask<Void, Void, StatusCode> {

        private final Job mJob;

        JobRequestTask(Job job) {
            mJob = job;
        }

        @Override
        protected void onPreExecute() {
            showProgress();
        }

        @Override
        protected StatusCode doInBackground(Void... params) {

            JobRequestProcess jobRequestProcess = new JobRequestProcess(mJob,getApplicationContext());
            StatusCode statusCode = jobRequestProcess.createJob();
            return statusCode;

        }

        @Override
        protected void onPostExecute(final StatusCode statusCode) {
            if(statusCode == StatusCode.SUCCESS) {
                Intent intent = new Intent(getApplicationContext(),BookingsActivity.class);
                startActivity(intent);
                //registerGcm();
                finish();
            }
            else {
                CustomToast.makeToast(3, ExpressBookingActivity.this.getApplicationContext(), "Unable to create request at this time , please try in sometime",
                        ExpressBookingActivity.this);

            }
            cancelProgress();
        }

        @Override
        protected void onCancelled() {
            cancelProgress();
        }
    }


    private void showProgress() {
        ((RelativeLayout)findViewById(R.id.job_request_button)).setEnabled(false);
        ((TextView) findViewById(R.id.job_request_button_text)).setVisibility(View.GONE);
        ((ProgressBar) findViewById(R.id.job_request_button_progress)).setVisibility(View.VISIBLE);
    }

    private void cancelProgress() {
        ((RelativeLayout)findViewById(R.id.job_request_button)).setEnabled(true);
        ((TextView) findViewById(R.id.job_request_button_text)).setVisibility(View.VISIBLE);
        ((ProgressBar) findViewById(R.id.job_request_button_progress)).setVisibility(View.GONE);
    }
    private boolean validateData() {
        boolean isDataValid = true;
        if(jobType == JobType.HOME_SERVICE ) {
            EditText message = (EditText) findViewById(R.id.message);
            if (message.getText().toString().equals("")) {
                message.setError("Please enter what you need in service");
                isDataValid = false;
            }
        }

        EditText address = (EditText) findViewById(R.id.address);
        if(address.getText().toString().equals("")) {
            address.setError("Please enter where you need the service");
            isDataValid = false;
        }

        EditText locality = (EditText) findViewById(R.id.locality);
        if(locality.getText().toString().equals("")) {
            locality.setError("Please enter locality information");
            isDataValid = false;
        }
        //String LOCALRAMUPREF = "LocalRamuPref" ;
        //SharedPreferences sharedpreferences = getSharedPreferences(LOCALRAMUPREF, Context.MODE_PRIVATE);
        EditText mobile = (EditText) findViewById(R.id.mobile);
        //mobile.setText(sharedpreferences.getString("mobilenoKey",""));
        if(mobile.getText().toString().equals("")) {
            mobile.setError("Please enter your mobile number");
            isDataValid = false;
        }

        return isDataValid;
    }


    public void createJobRequest(View view) {

        if(!internetDetector.isConnectingToInternet()) {
            CustomToast.makeToast(3, getApplicationContext(), "No Internet. Please check the network setting", this);
            return;
        }

        if (validateData()) {
            String category = null;

            if(selectedCategory == null) {
                if(categoryName.isEnabled()){
                    Log.e("111****11111*","");
                    category= categoryName.getText().toString().trim();
                }else if(categorySpinner.isEnabled()){
                    Log.e("22****22222*","");
                    category = ((Spinner) findViewById(R.id.category_spinner)).getSelectedItem().toString();
                }else{
                    Toast.makeText(getApplicationContext(),"LLL",Toast.LENGTH_LONG).show();
                }
            } else {
                category = selectedCategory;
            }

            String jobDetails = ((EditText) findViewById(R.id.message)).getText().toString();
            String address = ((EditText) findViewById(R.id.address)).getText().toString();
            String locality = ((EditText) findViewById(R.id.locality)).getText().toString();
            String landmark = ((EditText) findViewById(R.id.landmark)).getText().toString();
            String mobile = ((EditText) findViewById(R.id.mobile)).getText().toString();
            SharedPreferences sharedpreferences = getSharedPreferences(LOCALRAMUPREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("mobileExpress",mobile);
            editor.putString("houseExpress",address);
            editor.putString("landmarkExpress",landmark);
            editor.commit();

            if(jobDetails != null && jobDetails.equals("")) {
                jobDetails = String.valueOf(extractExtraJobInfo());
            }
            String requestDate, requestTime;
            String serviceTime = ((Spinner) findViewById(R.id.when)).getSelectedItem().toString(); {
                if(serviceTime.equals("Now")) {
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    String fromTime  = sdf.format(cal.getTime());
                    cal.add(Calendar.HOUR, 2);
                    String toTime =  sdf.format(cal.getTime());
                    requestDate = "Today";
                    requestTime = String.format("%s-%s", fromTime,toTime);
                } else {
                    requestDate = ((Spinner) findViewById(R.id.date)).getSelectedItem().toString();
                    requestTime = ((Spinner) findViewById(R.id.time_range)).getSelectedItem().toString();
                }
            }

            UserInfo userInfo = Utilities.getUserInfo(getApplicationContext());
            if(userInfo == null) {
                Intent intent = new Intent(this, UserLoginActivity.class);
                startActivity(intent);
                return;
            }

            Address selectedAddress = GeneralDefs.currentSelectedAddress;
            if(selectedAddress == null) {
                Intent intent = new Intent(this, AddressSearchActivity.class);
                startActivityForResult(intent, ADDRESS_FIND_REQUEST);
                return;
            } else {
                selectedAddress.setAddressPrimary(address);
                selectedAddress.setLandmark(landmark);
                selectedAddress.setMobileNumber(mobile);
                Utilities.updateSavedAddress(selectedAddress, getApplicationContext());
            }

            int priceQuote = 0;
            if(jobType == JobType.WATER_CAN_DELIVERY) {
                priceQuote = calculatePrice();
            }
            Job job = jobFactory.buildExpressJob(category, null, userInfo.getUserId(),
                    String.format("%s, %s, Landmark - %s", address,locality,landmark),
                    null, selectedAddress.getLatitude(), selectedAddress.getLongitude(), mobile,
                    jobDetails, requestDate, requestTime, jobType, extractExtraJobInfo(), priceQuote);

            new JobRequestTask(job).execute();

        }
    }

    private void handleWaterCan() {
        categorySpinner.setEnabled(false);
        ((CardView) findViewById(R.id.water_bottle_panel)).setVisibility(View.VISIBLE);

        ((CardView) findViewById(R.id.category_panel)).setVisibility(View.GONE);
        ((CardView) findViewById(R.id.message_panel)).setVisibility(View.GONE);

        AppConfig appConfig = Utilities.getAppConfig(getApplicationContext());
        waterPrices = appConfig.getWaterCanPrice();

        Spinner waterCanType = ((Spinner) findViewById(R.id.water_brand_spinner));
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getWaterPriceList());
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        waterCanType.setAdapter(spinnerArrayAdapter);
        getSupportActionBar().setTitle("Water Can Delivery");
    }
    private void handleLaundry() {
        categorySpinner.setEnabled(false);
        ((CardView) findViewById(R.id.category_panel)).setVisibility(View.GONE);
        getSupportActionBar().setTitle("Laundry Service Request");
    }


    private Map<String , String> extractExtraJobInfo() {
        Map<String, String> extraJobInfo = new HashMap<String, String>();
        if(jobType != JobType.HOME_SERVICE) {
            switch (jobType) {
                case WATER_CAN_DELIVERY:
                    extraJobInfo.put(JobExtraParamKeys.WATER_CAN_BRAND,
                            ((Spinner) findViewById(R.id.water_brand_spinner)).getSelectedItem().toString());
                    extraJobInfo.put(JobExtraParamKeys.WATER_CAN_QUANTITY,
                            ((Spinner) findViewById(R.id.water_quantity_spinner)).getSelectedItem().toString());
                    extraJobInfo.put(JobExtraParamKeys.WATER_DELIVERY_FLOOR_NUMBER,
                            ((Spinner) findViewById(R.id.floor_number_spinner)).getSelectedItem().toString());
                    break;
                default:
            }

        }

        return extraJobInfo;
    }

    private int calculatePrice() {
        int unitPrice = 0;
        int selectedBrand = ((Spinner) findViewById(R.id.water_brand_spinner)).getSelectedItemPosition();
        int selectedQuantity = ((Spinner) findViewById(R.id.water_quantity_spinner)).getSelectedItemPosition();
        if(waterPrices != null && selectedBrand < waterPrices.size()) {
            unitPrice = waterPrices.get(selectedBrand).getPrice();
        }
        return unitPrice * (selectedQuantity + 1);
    }

    private void displayOffer(OfferSettingModel offerSettingModel) {

        if(offerSettingModel == null || !offerSettingModel.isEnabled()) {
            return;
        }
        Context appContext = this.getApplicationContext();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(offerSettingModel.getPrimaryText());

        // set dialog message
        alertDialogBuilder
                .setMessage(offerSettingModel.getSecondaryText())
                .setCancelable(false)
                .setPositiveButton("Got It", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    private int getCurrentHour() {
        Calendar calender = Calendar.getInstance();
        calender.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        Log.i("Current Time in IST", "" + calender.get(Calendar.HOUR_OF_DAY));
        return calender.get(Calendar.HOUR_OF_DAY);
        //return 21;
    }

    private boolean isValidBookingTimeForToday() {
        int currentHour = getCurrentHour();
        return (currentHour >= 8 && currentHour <= 19);
    }
    private void disableBookNowOption() {
        ((CardView) findViewById(R.id.date_panel)).setVisibility(View.VISIBLE);
        ((CardView) findViewById(R.id.time_range_panel)).setVisibility(View.VISIBLE);
        ((CardView) findViewById(R.id.when_panel)).setVisibility(View.GONE);
    }

    private ArrayList<String> getWaterPriceList() {
        ArrayList<String> waterTypeArray = new ArrayList<String>();
        if(waterPrices != null) {
            for (WaterPriceModel waterPrice : waterPrices) {
                waterTypeArray.add(String.format("%s (Rs %s)", waterPrice.getBrand(), waterPrice.getPrice()));
            }
        }
        return waterTypeArray;
    }
}
